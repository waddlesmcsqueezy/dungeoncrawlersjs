import Direction from '../util/Direction';
import PathFinding from 'pathfinding';

function isBlocked(entity, target, game, direction) {
  if (entity.gridX() + direction.x == target.gridX() && entity.gridY() + direction.y == target.gridY()) {
    return true
  } else {return false}
}

function findPath(entity, target, game) {
  var gridBackup = game.world.grid.clone()
  var path = game.world.pathfinder.findPath(entity.gridX(), entity.gridY(), target.gridX(), target.gridY(), game.world.grid)
  entity.move(path[1][0] - entity.gridX(), path[1][1] - entity.gridY())
  game.world.grid = gridBackup
}

export default class TurnHandler {
  constructor(game) {
    this.action
    this.currentActor = 0

    this.pathfinder = new PathFinding.AStarFinder({
      allowDiagonal: true,
      dontCrossCorners: true
    });
    this.mapMatrix = this.mapToGrid(game);

    this.mapGrid = new PathFinding.Grid(this.mapMatrix)
  }

  findPath(origin, target) {
    var gridBackup = this.mapGrid.clone();
    var path = this.pathfinder.findPath(origin.getGrid().x, origin.getGrid().y, target.getGrid().x, target.getGrid().y, this.mapGrid);
    this.mapGrid = gridBackup;
    return path;
  }

  mapToGrid(game) {
    var grid = []
    for (var y = 0; y < game.world.map.height; y++) {
      for (var gridY = 0; gridY < game.world.map[y][0].height; gridY++) {
        grid.push([]);  //[], [], []
        for (var x = 0; x < game.world.map.width; x++) { // 0-2
          for (var gridX = 0; gridX < game.world.map[y][x].width; gridX++) { // 0-12
            if (game.world.map[y][x].getTile(gridX,gridY).isSolid) {
              grid[gridY + (game.world.map[y][0].height * y)].push(1);  //[1-39], [1-39]
            } else {grid[gridY + (game.world.map[y][0].height * y)].push(0);}
          } console.log(game.world.map[y][x].height)
        }
      }
    }
    console.log(grid)
    return grid;
  }

  proccess(game) {
    if (game.world.actors[this.currentActor].hasMoves() == false && game.world.actors[this.currentActor].isMoving == false) {
      if (game.world.actors[game.world.actors.length - 1].hasMoves() == false && game.world.actors[game.world.actors.length - 1].isMoving == false) {
        for (var i = 0; i < game.world.actors.length; i++) {
          game.world.actors[i].resetMoves();
        }
        this.currentActor = (this.currentActor + 1) % game.world.actors.length

      } else {this.currentActor = (this.currentActor + 1) % game.world.actors.length};  // if this actor is out of moves, go to next actor
    }

    if (game.world.actors[this.currentActor].needsInput == null && game.world.actors[this.currentActor].hasMoves()) { //get ai action
      if (game.world.actors[this.currentActor].isMoving == false) {
        // console.log("actor: " + this.currentActor + ", getting ai response...");
        game.world.actors[this.currentActor].getResponse(game, game.world.actors[this.currentActor]);  //get ai response
        var action = game.world.actors[this.currentActor].getAction();
      }
    }
    else if (game.world.actors[this.currentActor].needsInput != null && game.world.actors[this.currentActor].hasMoves()) { //get player input
      if (game.world.actors[this.currentActor].isMoving == false) {
        // console.log("getting player response...");
        game.world.actors[this.currentActor].getInput(game);               // poll input from player
        var action = game.world.actors[this.currentActor].getAction();     // poll action from player
      }
    }

    if (action == null) {return}                                         // return to top if no action has been set

    game.world.actors[this.currentActor].performAction();                // perform action

  }
}
