export default class CharacterClass {
    constructor(characterClass) {
        //fluff & stats
        this.name = characterClass.name
        this.description = characterClass.description

        this.agility = characterClass.baseAgility
        this.divinity = characterClass.baseDivinity
        this.heresy = characterClass.baseHeresy
        this.intuition = characterClass.baseIntuition
        this.strength = characterClass.baseStrength
        this.speech = characterClass.baseSpeech

        this.skills = new Map() //This is the player's skill tree

        this.quests = new Map()
        this.completedQuests = new Map()
        this.bounties = new Map()
        this.completedBounties = new Map()
    }
}
