// TODO: THIS ISN'T USED RIGHT NOW, probably an issue
export var CLASSES = {
    enemy: {
        name: "Enemy",
        description: "Some generic enemy.",
        baseAgility: 5,
        baseDivinity: 1,
        baseHeresy: 1,
        baseIntellect: 1,
        baseStrength: 5,
        baseSpeech: 5
    }
}
