export default class Quest {
    constructor(quest) {
        this.name = quest.name
        this.description = quest.description
        this.exp = quest.exp
        this.reward = quest.reward
        this.stages = quest.stages
        this.currentStage = 0
    }

    advanceStage() {
        this.currentStage += 1
    }

    checkComplete() {
        if (this.currentStage >= this.totalStages) {
            return true
        }
    }
}
