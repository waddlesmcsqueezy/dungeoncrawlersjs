import CharacterClass from '../actors/CharacterClass'

export default class LycanClass extends CharacterClass {
    constructor() {
        super({
            name: "Lycan",
            description: "Clawing from the depths of immortality, the lycan is the bane of all that is holy. A feeble warrior in their human form, the Lycan boasts extreme strength and dexterity when succumbing to the beast blood.",
            baseAgility: 15,
            baseDivinity: 1,
            baseHeresy: 5,
            baseIntellect: 2,
            baseStrength: 15,
            baseSpeech: 2
        })
    }

    proc() {
        if (skillLevel == 0) {
            if (this.health < 0.20 * this.health) {
                this.transformToBeast()
            }
        }
    }
}
