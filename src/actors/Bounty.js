export default class Bounty {
    constructor(bounty) {
        this.name = bounty.name
        this.description = bounty.description
        this.exp = bounty.exp
        this.reward = bounty.reward
        this.stages = bounty.stages
        this.currentStage = 0
    }

    advanceStage() {
        this.currentStage += 1
    }

    checkComplete() {
        if (completionState) {
            return true
        }
    }
}
