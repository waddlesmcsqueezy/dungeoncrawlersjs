import Actor from './Actor';
import Direction from '../util/Direction';
import Types from '../content/types';

export default class Player extends Actor {
  constructor(game, characterClass, sprite) {
    super(game, characterClass, sprite)
  }

  handleInput(game) {
    if (game.input.keyboard.lastKey != null) {
      if (game.input.keyboard.lastKey.justDown && this.isMoving == false) {
        switch (game.input.keyboard.lastKey.keyCode) {
          case Phaser.KeyCode.I:
            // if (game.world.guiGroup[1].visible) {
            //   game.world.guiGroup[1].toggle(game); // 1 is the action gui
            // }
            game.world.guiGroup[0].toggle(game); //0 is the inventory gui
            // game.world.guiGroup[2].toggle(game);
            break;
          case Phaser.KeyCode.A:
            if (game.world.guiGroup[0].visible) {
              game.world.guiGroup[0].toggle(game); //0 is the inventory gui
            }
            game.world.guiGroup[1].toggle(game); // 1 is the action gui
            break;
          case Phaser.KeyCode.CLOSED_BRACKET:
            if (game.world.actors[0].stanceHand == Types.equipSlot.RIGHT) {
              game.world.actors[0].toggleWeaponStance(); //swap from one handed to two handed mode if current hand is rightHand
            } else if (game.world.actors[0].stanceHand == Types.equipSlot.LEFT && game.world.actors[0].weaponStance == Types.weaponStances.ONEHANDED) {
              game.world.actors[0].toggleStanceHand();
              game.world.actors[0].toggleWeaponStance();
            } else if (game.world.actors[0].weaponStance == Types.weaponStances.TWOHANDED) { game.world.actors[0].toggleStanceHand(); }
            break;
          case Phaser.KeyCode.OPEN_BRACKET:
            if (game.world.actors[0].stanceHand == Types.equipSlot.LEFT) {
              game.world.actors[0].toggleWeaponStance(); //swap from one handed to two handed mode if current hand is rightHand
            } else if (game.world.actors[0].stanceHand == Types.equipSlot.RIGHT && game.world.actors[0].weaponStance == Types.weaponStances.ONEHANDED) {
              game.world.actors[0].toggleStanceHand();
              game.world.actors[0].toggleWeaponStance();
            } else if (game.world.actors[0].weaponStance == Types.weaponStances.TWOHANDED) { game.world.actors[0].toggleStanceHand(); }
            break;
          case Phaser.KeyCode.NUMPAD_0:
            this.setNextAction(this.actions.skipAction);
            break;
          case Phaser.KeyCode.NUMPAD_7:
            this.nextMoveDirection = Direction.NW;
            if (this.getTileAtDir(game, this.nextMoveDirection).walkable && this.getTileAtDir(game, Direction.N).walkable && this.getTileAtDir(game, Direction.W).walkable
              && this.isBlocked(this.nextMoveDirection) == false) {
              this.setNextAction(this.actions.moveAction);
              break;
            } else {
              console.log("can't move there");
              break;
            }

          case Phaser.KeyCode.NUMPAD_8:
            this.nextMoveDirection = Direction.N;
            if (this.getTileAtDir(game, this.nextMoveDirection).walkable && this.isBlocked(this.nextMoveDirection) == false) {
              this.setNextAction(this.actions.moveAction);
              break;
            } else {
              console.log("can't move there");
              break;
            }

          case Phaser.KeyCode.NUMPAD_9:
            this.nextMoveDirection = Direction.NE;
            if (this.getTileAtDir(game, this.nextMoveDirection).walkable && this.getTileAtDir(game, Direction.N).walkable && this.getTileAtDir(game, Direction.E).walkable
              && this.isBlocked(this.nextMoveDirection) == false) {
              this.setNextAction(this.actions.moveAction);
              break;
            } else {
              console.log("can't move there");
              break;
            }
          case Phaser.KeyCode.NUMPAD_4:
            this.nextMoveDirection = Direction.W;
            if (this.getTileAtDir(game, this.nextMoveDirection).walkable && this.isBlocked(this.nextMoveDirection) == false) {
              this.setNextAction(this.actions.moveAction);
              break;
            } else {
              console.log("can't move there");
              break;
            }

          case Phaser.KeyCode.NUMPAD_6:
            this.nextMoveDirection = Direction.E;
            if (this.getTileAtDir(game, this.nextMoveDirection).walkable && this.isBlocked(this.nextMoveDirection) == false) {
              this.setNextAction(this.actions.moveAction);
              break;
            } else {
              console.log("can't move there");
              break;
            }

          case Phaser.KeyCode.NUMPAD_1:
            this.nextMoveDirection = Direction.SW;
            if (this.getTileAtDir(game, this.nextMoveDirection).walkable && this.getTileAtDir(game, Direction.S).walkable && this.getTileAtDir(game, Direction.W).walkable
              && this.isBlocked(this.nextMoveDirection) == false) {
              this.setNextAction(this.actions.moveAction);
              break;
            } else {
              console.log("can't move there");
              break;
            }

          case Phaser.KeyCode.NUMPAD_2:
            this.nextMoveDirection = Direction.S;
            if (this.getTileAtDir(game, this.nextMoveDirection).walkable
            && this.isBlocked(this.nextMoveDirection) == false) {
              this.setNextAction(this.actions.moveAction);
              break;
            } else {
              console.log("can't move there");
              break;
            }

          case Phaser.KeyCode.NUMPAD_3:
            this.nextMoveDirection = Direction.SE;
            if (this.getTileAtDir(game, this.nextMoveDirection).walkable && this.getTileAtDir(game, Direction.S).walkable && this.getTileAtDir(game, Direction.E).walkable
              && this.isBlocked(this.nextMoveDirection) == false) {
              this.setNextAction(this.actions.moveAction);
              break;
            } else {
              console.log("can't move there");
              break;
            }
        }
      }
    } else {return}
  }

  needsInput() {
    return true
  }

  getInput(game) {
    this.handleInput(game)
  }
}
