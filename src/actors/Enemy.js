import Actor from '../actors/Actor';
import Direction from '../util/Direction';

export default class Enemy extends Actor {
  constructor(game, characterClass, sprite) {
    super(game, characterClass, sprite);
    this.aggroRange = 9; // this is the distance at which the ai will follow the player and track/hunt them down
    this.isTrackingTarget = false
  }

  getTarget(game) {
    return game.world.actors[0]
  }

  getDirOfTarget() {
    var distance = this.gridDistance(this.getTarget());
    console.log("distance: " + distance.x + ", " + distance.y)
    var dir = {};
    switch(true) {
      case (distance.x == 0):
      console.log("x = 0");
      dir.x = 0;
      break;

      case (distance.x < 0):
      console.log("x = 1");
      dir.x = 1;
      break;

      case (distance.x > 0):
      console.log("x = -1");
      dir.x = -1;
      break;
    }
    switch(true) {
      case (distance.y == 0):
      console.log("y = 0");
      dir.y = 0;
      break;

      case (distance.y < 0):
      console.log("y = 1");
      dir.y = 1;
      break;

      case (distance.y > 0):
      console.log("y = -1");
      dir.y = -1;
      break;
    }
    return dir;
  }

  getResponse(game) {
    if (this.gridDistance(game.world.actors[0]).x <= this.aggroRange && this.gridDistance(game.world.actors[0]).y <= this.aggroRange) {
      this.setTarget(game.world.actors[0]);        //   ^ check if actor is within aggro distance of player ^
      console.log("within distance of player");

      var path = game.world.turnHandler.findPath(this, this.getTarget());

      if (this.isAdjacentTo(this.getTarget(game))) {
        console.log("is adjacent to target (player)");
        this.setNextAction(this.actions.skipAction);
      } else {
        if (path.length > 0) {
          this.nextMoveDirection = {x: path[1][0] - this.getGrid().x, y: path[1][1] - this.getGrid().y};
          if (this.isBlocked(this.nextMoveDirection)) {
            console.log("path is blocked");
            this.setNextAction(this.actions.skipAction);
          } else {
            this.setNextAction(this.actions.moveAction);
          }
        } else {
          console.log("cannot find path to target");
          this.setNextAction(this.actions.skipAction);
        }
      }
    } else {this.setTarget(null);}
  }

  getTarget() {
    return null
  }

  setTarget(target) {
    this.getTarget = function() {
      return target
    }
  }
}
