import CharacterClass from '../actors/CharacterClass'

export default class EnemyClass extends CharacterClass {
    constructor() {
        super({
                name: "Generic enemy",
                baseAgility: 8,
                baseDivinity: 4,
                baseHeresy: 4,
                baseIntellect: 4,
                baseStrength: 3,
                baseSpeech: 4
            })
    }

    proc() {
        if (skillLevel == 0) {

        }
    }
}
