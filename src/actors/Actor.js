import Container from '../items/Container';
import ItemSlot from '../items/ItemSlot';
import Types from '../content/Types';
import Direction from '../util/Direction';

export default class Actor {
  constructor(game, characterClass, sprite) {

  	this.characterClass = characterClass;

    this.currentHealth = this.getMaxHealth();
    
    // this.maxWeight = (((this.characterClass.strength + 3) * 15) / 1.5) NOT USING WEIGHT AT THE MOMENT FOR INV, USING SLOTS
    this.inventory = new Container();

    this.spells = new Map(); //Player's current collection of spells. Can only have 1 of each spell, infinite uses.

    this.equippedItems = {};

    this.weaponStance = Types.weaponStances.ONEHANDED;
    this.stanceHand = Types.equipSlot.LEFT;

    this.equippedItems.rightHand = new ItemSlot();
    this.equippedItems.leftHand = new ItemSlot();
    this.equippedItems.head = new ItemSlot();
    this.equippedItems.torso = new ItemSlot();
    this.equippedItems.leg = new ItemSlot();
    this.equippedItems.ammo = new ItemSlot();

    //turn based logic
    this.movesLeft = this.getMaxMoves();
    this.nextAction;
    this.nextMoveDirection;

    //visual stuff
    this.isMoving = false;
    this.sprite = sprite;
    this.sprite.smoothed = false;

    this.actions = {
      moveAction : { //         -90 = left        +90 = right
        moveCost: 1, // amount of moves this action uses
        use : function (game, actor) { // command sprite to move x & y amount of tiles // TODO: fix scoping and no longer require actor as a parameter in this function.
          actor.isMoving = true
          game.add.tween(actor.sprite).to({
              x: actor.sprite.x + (actor.nextMoveDirection.x * game.world.WORLD_SCALE),
              y: actor.sprite.y + (actor.nextMoveDirection.y * game.world.WORLD_SCALE)
            }, 250,
            Phaser.Easing.Quadratic.InOut, true).onComplete.add(function() {
            actor.isMoving = false
          }, actor)
        }
      },

      skipAction : { //         -90 = left        +90 = right
        moveCost: 1, // amount of moves this action uses
        use : function (game, actor) { // command sprite to move x & y amount of tiles
          console.log(actor.characterClass.name + " is skipping their turn.")
        }
      }
    }
  }

  damage(damageAmount) {
    if (this.currentHealth - damageAmount < 0) {
      this.currentHealth = 0;
    } else {this.currentHealth -= damageAmount}
  }

  getActions() {
    var actions = [];
    // this will get actions from equipped items
    // for (var i = 0; i < this.inventory.getItems().length; i++) {
    //   // console.log(this.inventory.getItems()[i][1].actions)
    //   var action = this.inventory.getItems()[i][1].actions;
    //
    //   action.itemKey = this.inventory.getItems()[i][0];
    //   action.item = this.inventory.getItems()[i][1];
    //   // action.item.actions = null;
    //   actions.push(action);
    // }
    switch(true) {
      case this.weaponStance == Types.weaponStances.ONEHANDED:
        if (this.equippedItems.rightHand.getItem() == null) {
          console.log("No Item");
        } else {
          for(var i = 0; i < this.equippedItems.rightHand.getItem()[1].actions.oneHanded.length; i++) {
            actions.push(this.equippedItems.rightHand.getItem()[1].actions.oneHanded[i]);
          }
        }
        if (this.equippedItems.leftHand.getItem() == null) {
          console.log("No Item");
        } else {
          for(var i = 0; i < this.equippedItems.leftHand.getItem()[1].actions.oneHanded.length; i++) {
            actions.push(this.equippedItems.leftHand.getItem()[1].actions.oneHanded[i]);
          }
        }
        break;
      case this.weaponStance == Types.weaponStances.TWOHANDED:
        switch(true) {
          case this.stanceHand == Types.equipSlot.LEFT:
            if (this.equippedItems.leftHand.getItem() == null) {
              console.log("No Item");
            } else {
              for(var i = 0; i < this.equippedItems.leftHand.getItem()[1].actions.twoHanded.length; i++) {
                actions.push(this.equippedItems.leftHand.getItem()[1].actions.twoHanded[i]);
              }
            }
            break;
          case this.stanceHand == Types.equipSlot.RIGHT:
            if (this.equippedItems.rightHand.getItem() == null) {
              console.log("No Item");
            } else {
              for(var i = 0; i < this.equippedItems.rightHand.getItem()[1].actions.twoHanded.length; i++) {
                actions.push(this.equippedItems.rightHand.getItem()[1].actions.twoHanded[i]);
              }
            }
            break;
        }
        break;
    }
    return actions;
  }

  getMaxHealth() {
    return Math.round(((this.characterClass.strength) + 1) * 5);
  }

  getHealthRegen() {
    return Math.round(((this.characterClass.strength) + 1) / 3);
  }

  getMaxStamina() {
    return Math.round((((this.characterClass.agility * 1.6) + this.characterClass.strength / 1.4) + 2) / 1.1);
  }

  getStaminaRegen() {
    return Math.round(((this.characterClass.agility) + 1) / 3);
  }

  getPhysicalResistance(strengthMod) {
    if (strengthMod != null) {
      return (this.characterClass.strength + strengthMod) * 1.5;
    } else { return this.characterClass.strength * 1.5 }
  }

  getMaxMoves() {
    if (Math.round((this.characterClass.agility + 3) / 7) < 1) {
        return 1;
    } else { return Math.round((this.characterClass.agility + 3) / 7) }
  }

  getActorStat(weaponStat) {
    switch (weaponStat) {
      case Types.stats.AGILITY:
        return this.characterClass.agility;
        break;
      case Types.stats.DIVINITY:
        return this.characterClass.divinity;
        break;
      case Types.stats.HERESY:
        return this.characterClass.heresy;
        break;
      case Types.stats.INTUITION:
        return this.characterClass.intuition;
        break;
      case Types.stats.STRENGTH:
        return this.characterClass.strength;
        break;
      case Types.stats.SPEECH:
        return this.characterClass.speech;
        break;
      case Types.stats.HEALTH:
        return this.characterClass.currentHealth;
        break;
      case Types.stats.MAXHEALTH:
        return this.getMaxHealth();
        break;
      case Types.stats.HEALTHREGEN:
        return this.getHealthRegen();
        break;
      case Types.stats.MAXSTAMINA:
        return this.getMaxStamina();
        break;
      case Types.stats.STAMINAREGEN:
        return this.getStaminaRegen();
        break;
    }
  }

  addSpell(spellId) {
    this.spells.set(spellId, Spells.get(spellId));
  }

  setWeaponStance(stance) { //change stance (one or two handed) Please make sure to use one of the Types.weaponStances enums (weaponStances.ONEHANDED or weaponStances.TWOHANDED)
    this.weaponStance = stance;
  }

  setStanceHand(hand) {
    this.stanceHand = hand;
  }

  toggleWeaponStance() {
    switch(true) {
      case this.weaponStance == Types.weaponStances.ONEHANDED:
        this.setWeaponStance(Types.weaponStances.TWOHANDED);
        break;
      case this.weaponStance == Types.weaponStances.TWOHANDED:
        this.setWeaponStance(Types.weaponStances.ONEHANDED);
        break;
    }
  }

  toggleStanceHand() {
    switch(true) {
      case this.stanceHand == Types.equipSlot.LEFT:
        this.setStanceHand(Types.equipSlot.RIGHT);
        break;
      case this.stanceHand == Types.equipSlot.RIGHT:
        this.setStanceHand(Types.equipSlot.LEFT);
        break;
      }
    }

  getGrid(direction) { //optionally - input a direction from the Directions variable ex: Directions.NW - else returns current grid location
    if (direction == null) {
      return {x: this.gridX(), y: this.gridY()}
    } else {
      var xFinal = this.gridX() + direction.x;
      var yFinal = this.gridY() + direction.y;
      return {x: xFinal, y: yFinal}
    }
  }

  getTileAtDir(game, dir) { // pass in a Direction; eg: Direction.NW
    return game.world.turnHandler.mapGrid.nodes[this.getGrid().y + dir.y][this.getGrid().x + dir.x];
  }

  setPos(x, y) { //set this sprite's position to specified tile
    this.sprite.position.setTo(x * game.world.WORLD_SCALE, y * game.world.WORLD_SCALE);
  }

  move(direction, game) {
    this.setNextAction(this.Actions.moveAction.use(direction, game));
  }

  useMoves(amount) {
    this.movesLeft -= amount;
  }

  resetMoves() {
    this.movesLeft = this.getMaxMoves();
  }

  hasMoves() {
    if (this.movesLeft > 0) {
      return true;
    } else {return false}
  }

  setNextAction(action) {
    this.nextAction = action;
  }

  getAction() {
    var action = this.nextAction;
    return action;
  }

  resetAction() {
    this.nextAction = null;
    this.nextMoveDirection = null;
  }

  performAction() {
    var action = this.getAction()
    action.use(game, this);
    this.useMoves(this.getAction().moveCost);
    this.resetAction();
  }

  gridX() {
    return Math.round(this.sprite.x / game.world.WORLD_SCALE);
  }

  gridY() {
    return Math.round(this.sprite.y / game.world.WORLD_SCALE);
  }

  isAdjacentTo(targetActor) {
    if ((this.gridX() - targetActor.gridX() == -1 && this.gridY() - targetActor.gridY() == -1) || // nw
        (this.gridX() - targetActor.gridX() == 0 && this.gridY() - targetActor.gridY() == -1)  || // n
        (this.gridX() - targetActor.gridX() == 1 && this.gridY() - targetActor.gridY() == -1)  || // ne
        (this.gridX() - targetActor.gridX() == -1 && this.gridY() - targetActor.gridY() == 0)  || // w
        (this.gridX() - targetActor.gridX() == 1 && this.gridY() - targetActor.gridY() == 0)   || // e
        (this.gridX() - targetActor.gridX() == -1 && this.gridY() - targetActor.gridY() == 1)  || // sw
        (this.gridX() - targetActor.gridX() == 0 && this.gridY() - targetActor.gridY() == 1)   || // s
        (this.gridX() - targetActor.gridX() == 1 && this.gridY() - targetActor.gridY() == 1))     // se
    {
      return true
    } else {return false}
  }

  isBlocked(dir) {
    console.log(dir.x + this.getGrid().x, dir.y + this.getGrid().y);
    for(var i = 0; i < game.world.actors.length; i++) {
      console.log(game.world.actors[i].getGrid());
      if (game.world.actors[i].getGrid().x == dir.x + this.getGrid().x && game.world.actors[i].getGrid().y == dir.y + this.getGrid().y) {
        return true;
      }
    }

    return false;
  }

  gridDistance(targetActor) { // calculate tile difference inbetween this actor and targetActor
    var gridDifference = this.getGrid();
    gridDifference.x -= targetActor.getGrid().x;
    gridDifference.y -= targetActor.getGrid().y;
    return gridDifference;
  }
}
