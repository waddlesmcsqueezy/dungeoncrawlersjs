let Types = {}

Types.damageTypes = {
	PHYSICAL: "Physical",
	MAGIC: "Magic",
	PIERCING: "Piercing",
	FIRE: "Fire",
	POISON: "Poison",
	BLEED: "Laceration",
	TRAUMA: "Trauma",
	ELECTRICITY: "Electric",
	DARK: "Nightmare",
	DIVINE: "Holy"
}

Types.stats = {
	AGILITY: "agility",
	DIVINITY: "divinity",
	HERESY: "heresy",
	INTELLECT: "intellect",
	LUCK: "luck",
	STRENGTH: "strength",
	SPEECH: "speech",
	HEALTH: "currentHealth",
	MAXHEALTH: "maxHealth",
	HEALTHREGEN: "healthRegen",
	MAXSTAMINA: "maxStamina",
	STAMINAREGEN: "staminaRegen"
},

Types.itemTypes = { // editing/adding to this will require rewriting source code. please leave this alone unless you know what you're doing.
	WEAPON: "Weapon", // weapon
	HEAD: "Head", //head armor item
	TORSO: "Torso", //head armor item
	LEG: "Leg", //head armor item
	AMMO: "Head", //ammo item
	FOOD: "Food", //food item
	AID: "Aid", // aid/health item
	DRUG: "Drug", //drug
	PART: "Part", //crafting ingredient
	QST: "Quest" //quest item
},

Types.weaponCategories = {
	SHORTSWORD: "Shortsword",
	LONGSWORD: "Longsword",
	GREATSWORD: "Greatsword",
	AXE: "Axe",
	GREATAXE: "Greataxe",
	POLEARM: "Polearm",
	KNIFE: "Knife",
	BLUNT: "Blunt",
	FIST: "Fist",
	SMG: "Submachinegun",
	PDW: "Personal Defense Weapon",
	AR: "Assault Rifle",
	CARBINE: "Carbine",
	BR: "Battle Rifle",
	SAR: "Semi-Auto Rifle",
	RR: "Revolver Rifle",
	LAR: "Lever Action Rifle",
	DMR: "Designated Marksman Rifle",
	SR: "Sniper Rifle",
	AMR: "Anti-Materiel Rifle",
	HANDGUN: "Handgun",
	REVOLVER: "Revolver",
	MP: "Machine Pistol",
	SHOTGUN: "Shotgun",
	GL: "Grenade Launcher",
	RPG: "Rocket Launcher",


},

Types.armorCategories = {
	HEAVY: "Heavy",
	MEDIUM: "Medium",
	LIGHT: "Light"
}

Types.potionTypes = {
  HEALTH: 1,
  BUFF: 2
},

Types.itemTiers = { 												// weight determines the chance that this tier of item will be spawned as loot for the player.
  WHITE: {color: "#FFFFFF", weight: 1},			// weight cont. | higher = higher % chance - lower = lower % chance.
  GREEN: {color: "#65FF00", weight: 1},			// color is a hexidecimal color code used to determine of an item's name in game.
  BLUE: {color: "#5442FF", weight: 1},
  PURPLE: {color: "#F442FF", weight: 1},
  PINK: {color: "#FF42B4", weight: 1},
  RED: {color: "#FF0000", weight: 1},
  ORANGE: {color: "#FFAA00", weight: 1}
},

Types.abilityTypes = {
  PROC: "Proc",
  PASSIVE: "Passive",
  ACTIVE: "Active"
},

Types.spellCategories = {
  HOLY: "Holy",
  UNHOLY: "Unholy",
  SORCERY: "Sorcery"
},

Types.effectApplyTypes = {
  DAMAGE: "Damage",
	DOT: "Damage Over Time",
  HEAL: "Deal",
  BUFF: "Buff",
  ENVIROMENT: "Enviroment",
  SPECIAL: "Special"
},

/*
DO NOT EDIT BELOW THIS LINE IF YOU ARE MODDING
DO NOT EDIT BELOW THIS LINE IF YOU ARE MODDING
DO NOT EDIT BELOW THIS LINE IF YOU ARE MODDING
*/

Types.buffApplyTypes = {
	MULTIPLY: "Multiply",
	ADDITION: "Addition"
},

Types.equipSlot = { // used for determining which hands/weapons the player is using
  RIGHT: "R",
  LEFT: "L",
	TORSO: "T",
	HEAD: "H",
	LEG: "L",
	AMMO: "A"
}

Types.weaponStances = { // used for determining hand and stance
	ONEHANDED: 1,
	TWOHANDED: 2
}

export default Types
