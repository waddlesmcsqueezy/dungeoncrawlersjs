var Calculations = {}
Calculations.DamageRatings = {
	S: 3.0,
	AAA: 2.5,
	AA: 2.3,
	A: 2.0,
	B: 1.7,
	C: 1.5,
	D: 1.3,
	E: 1.2,
	F: 1.1
}

export default Calculations
