import Types from './Types';
import Effects from './StatusEffects';
import Calculations from './Calculations';
import Attacks from './Attacks';
import InventoryOptions from './InventoryOptions';
import Sounds from './Sounds';

/*
 ________  _________  ______   ___ __ __   ______
/_______/\/________/\/_____/\ /__//_//_/\ /_____/\
\__.::._\/\__.::.__\/\::::_\/_\::\| \| \ \\::::_\/_
   \::\ \    \::\ \   \:\/___/\\:.      \ \\:\/___/\
   _\::\ \__  \::\ \   \::___\/_\:.\-/\  \ \\_::._\:\
  /__\::\__/\  \::\ \   \:\____/\\. \  \  \ \ /____\:\
  \________\/   \__\/    \_____\/ \__\/ \__\/ \_____\/
*/

let Items = {};

Items.BASE_ITEM = {
	type : Types.itemTypes.QST,
	name : "Item001",
	description : "Description001",
	value : 1,
	weight : 1,
	tier : Types.itemTiers.WHITE,
	sprite : "error"
}

/*    ___           ___           ___           ___         ___           ___           ___
     /__/\         /  /\         /  /\         /  /\       /  /\         /__/\         /  /\
    _\_ \:\       /  /:/_       /  /::\       /  /::\     /  /::\        \  \:\       /  /:/_
   /__/\ \:\     /  /:/ /\     /  /:/\:\     /  /:/\:\   /  /:/\:\        \  \:\     /  /:/ /\
  _\_ \:\ \:\   /  /:/ /:/_   /  /:/~/::\   /  /:/~/:/  /  /:/  \:\   _____\__\:\   /  /:/ /::\
 /__/\ \:\ \:\ /__/:/ /:/ /\ /__/:/ /:/\:\ /__/:/ /:/  /__/:/ \__\:\ /__/::::::::\ /__/:/ /:/\:\
 \  \:\ \:\/:/ \  \:\/:/ /:/ \  \:\/:/__\/ \  \:\/:/   \  \:\ /  /:/ \  \:\~~\~~\/ \  \:\/:/~/:/
  \  \:\ \::/   \  \::/ /:/   \  \::/       \  \::/     \  \:\  /:/   \  \:\  ~~~   \  \::/ /:/
   \  \:\/:/     \  \:\/:/     \  \:\        \  \:\      \  \:\/:/     \  \:\        \__\/ /:/
    \  \::/       \  \::/       \  \:\        \  \:\      \  \::/       \  \:\         /__/:/
     \__\/         \__\/         \__\/         \__\/       \__\/         \__\/         \__\/
*/

Items.weapons.guns.VECTOR = {
	type : Types.itemTypes.WEAPON, // type of item
	name : "KROSS Ultra V Euclidean", // name of item
	description : "A rusty blade.", // description
	value : 25, //money value
	category : Types.weaponCategories.SMG,
	// weight : 3,
	tier : Types.itemTiers.WHITE, // item color tier/rarity
	sprite : "error", // sprite
	damage : 5, // base weapon damage
	accuracy : 85, // base accuracy percentage; 0.01 - 1.00
	range : 1, // range of weapon attacks in tiles
	skill1 : Types.stats.STRENGTH, // which stat this weapon scales with.
	skill1Scaling : Calculations.DamageRatings.E, // how much this stat scales the weapon's damage
	actions : {
		oneHanded : [
			Attacks.slash,
			Attacks.thrust
		],
		twoHanded : [
			Attacks.slash,
			Attacks.thrust,
			Attacks.slam
		]
	},
	sounds : {
		attack : Sounds.weapons.guns.QUIET;
		equip : 'error';
	}
}

Items.weapons.melee.OLD_SWORD = {
	type : Types.itemTypes.WEAPON, // type of item
	name : "Old Sword", // name of item
	description : "A rusty blade.", // description
	value : 25, //money value
	category : Types.weaponCategories.SHORTSWORD,
	// weight : 3,
	tier : Types.itemTiers.WHITE, // item color tier/rarity
	sprite : "error", // sprite
	damage : 5, // base weapon damage
	accuracy : 85, // base accuracy percentage; 0.01 - 1.00
	range : 1, // range of weapon attacks in tiles
	skill1 : Types.stats.STRENGTH, // which stat this weapon scales with.
	skill1Scaling : Calculations.DamageRatings.E, // how much this stat scales the weapon's damage
	actions : {
		oneHanded : [
			Attacks.slash,
			Attacks.thrust
		],
		twoHanded : [
			Attacks.slash,
			Attacks.thrust,
			Attacks.slam
		]
	},
},

Items.weapons.melee.OLD_WOOD_AXE = {
	type : Types.itemTypes.WEAPON, // type of item
	name : "Old Axe", // name of item
	description : "A rusty, old axe. Good for chopping down trees. Well, not anymore at least.", // description
	value : 25, //money value
	category : Types.weaponCategories.AXE,
	// weight : 3,
	tier : Types.itemTiers.WHITE, // item color tier/rarity
	sprite : "error", // sprite
	damage : 7, // base weapon damage
	accuracy : 75, // base accuracy percentage; 0.01 - 1.00
	range : 1, // range of weapon attacks in tiles
	skill1 : Types.stats.STRENGTH, // which stat this weapon scales with.
	skill1Scaling : Calculations.DamageRatings.E, // how much this stat scales the weapon's damage
	actions : {
		oneHanded : [
			Attacks.slash,
		],
		twoHanded : [
			Attacks.slash,
			Attacks.slam,
			Attacks.powerSwing
		]
	}
},

Items.weapons.melee.ORANGE_SWORD_FIRE = {
	type : Types.itemTypes.WEAPON,
	name : "The Flame of Damnation",
	description : "A sword born in the heart of an ancient dragon. It's ember resonates with the soul of a Great Lord.",
	value : 1500,
	category : Types.weaponCategories.LONGSWORD,
	// weight : 4,
	tier : Types.itemTiers.ORANGE,
	sprite : "error",
	damage : 20,
	accuracy : 95,
	range : 1,
	skill1 : Types.stats.STRENGTH,
	skill1Scaling : Calculations.DamageRatings.C,
	skill2 : Types.stats.AGILITY,
	skill2Scaling : Calculations.DamageRatings.C,
	actions : {
		oneHanded : [
			Attacks.slash,
			Attacks.thrust
		],
		twoHanded : [
			Attacks.slash,
			Attacks.thrust,
			Attacks.slam
		]
	}
}

/*
          _____                    _____                    _____                   _______                   _____
         /\    \                  /\    \                  /\    \                 /::\    \                 /\    \
        /::\    \                /::\    \                /::\____\               /::::\    \               /::\    \
       /::::\    \              /::::\    \              /::::|   |              /::::::\    \             /::::\    \
      /::::::\    \            /::::::\    \            /:::::|   |             /::::::::\    \           /::::::\    \
     /:::/\:::\    \          /:::/\:::\    \          /::::::|   |            /:::/~~\:::\    \         /:::/\:::\    \
    /:::/__\:::\    \        /:::/__\:::\    \        /:::/|::|   |           /:::/    \:::\    \       /:::/__\:::\    \
   /::::\   \:::\    \      /::::\   \:::\    \      /:::/ |::|   |          /:::/    / \:::\    \     /::::\   \:::\    \
  /::::::\   \:::\    \    /::::::\   \:::\    \    /:::/  |::|___|______   /:::/____/   \:::\____\   /::::::\   \:::\    \
 /:::/\:::\   \:::\    \  /:::/\:::\   \:::\____\  /:::/   |::::::::\    \ |:::|    |     |:::|    | /:::/\:::\   \:::\____\
/:::/  \:::\   \:::\____\/:::/  \:::\   \:::|    |/:::/    |:::::::::\____\|:::|____|     |:::|    |/:::/  \:::\   \:::|    |
\::/    \:::\  /:::/    /\::/   |::::\  /:::|____|\::/    / ~~~~~/:::/    / \:::\    \   /:::/    / \::/   |::::\  /:::|____|
 \/____/ \:::\/:::/    /  \/____|:::::\/:::/    /  \/____/      /:::/    /   \:::\    \ /:::/    /   \/____|:::::\/:::/    /
          \::::::/    /         |:::::::::/    /               /:::/    /     \:::\    /:::/    /          |:::::::::/    /
           \::::/    /          |::|\::::/    /               /:::/    /       \:::\__/:::/    /           |::|\::::/    /
           /:::/    /           |::| \::/____/               /:::/    /         \::::::::/    /            |::| \::/____/
          /:::/    /            |::|  ~|                    /:::/    /           \::::::/    /             |::|  ~|
         /:::/    /             |::|   |                   /:::/    /             \::::/    /              |::|   |
        /:::/    /              \::|   |                  /:::/    /               \::/____/               \::|   |
        \::/    /                \:|   |                  \::/    /                 ~~                      \:|   |
         \/____/                  \|___|                   \/____/                                           \|___|
*/

Items.armor.heavy.WHITE_METAL_001 = {
	type : Types.itemTypes.TORSO,
	name : "Rusty Chestplate",
	description : "An antique. Found hanging over a fireplace.",
	value : 25,
	category : Types.armorCategories.HEAVY,
	tier : Types.itemTiers.WHITE,
	sprite : "error",
	protection : 5,
	resistance : 10
},

Items.armor.medium.WHITE_CHAIN_001 = {
	type : Types.itemTypes.TORSO,
	name : "Rusty Chainmail",
	description : "An old piece of armor. Simple, but effective. Mostly.",
	value : 25,
	category : Types.armorCategories.MEDIUM,
	tier : Types.itemTiers.WHITE,
	sprite : "error",
	protection : 8,
	resistance : 8
},

export default Items;
