var InventoryOptions = {};

InventoryOptions.equip = {
  title : "Equip",
  use : function (game, actor, slot, item) {
    if (slot.getItem == null) {
      slot.equipItem(item, actor.inventory);
    } else { throw "slot already has an item"; }
  }
}

InventoryOptions.unEquip = {
  title : "Equip",
  use : function (game, actor, slot, item) {
    if (slot.getItem != null && slot.getItem[0] == item[0]) {
      slot.unEquipItem(item[0]);
    } else { throw "there is no item/trying to unequip incorrect item"; }
  }
}

export default InventoryOptions
