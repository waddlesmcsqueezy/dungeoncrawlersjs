import Types from './Types'
import Calculations from './Calculations'

var Effects = {};

Effects.fire = {
	name: 'Ember',
	description: "Burns flesh very well.",
	color: "#ff6600",
	type: Types.effectApplyTypes.DOT,
	damage: 8,			//Base damage
	apply: function (caster, target) {
		target.health -= Math.round(((caster.intuition * Calculations.DamageRatings.C) + (caster.divinity * Calculations.DamageRatings.E) / 2) + this.damage);
	}
}

Effects.light = {
	name: 'Light',
	description: "The damage of one that is holy.",
	color: "#ccff66",
	type: Types.effectApplyTypes.DAMAGE,
	damage: 5,			//Base damage
	apply: function (caster, target) {
		target.health -= Math.round((caster.divinity * Calculations.DamageRatings.B) /2  + this.damage);
	}
}

Effects.curse = {
	name: 'Curse',
	description: "Incantation of one who is attuned to the dark arts.",
	color: "#000066",
	type: Types.effectApplyTypes.DAMAGE,
	damage: 10,			//Base damage
	apply: function (caster, target) {
		target.health -= Math.round(((caster.intuition * Calculations.DamageRatings.E) + (caster.heresy * Calculations.DamageRatings.D) / 2) + this.damage);
	}
}

Effects.slash = {
	name: 'Slash',
	description: "Opening wounds on one's enemy creates despair.",
	color: "",
	type: Types.effectApplyTypes.DAMAGE,
	damage: 9,			//Base damage
	apply: function (caster, target) {
		target.health -= Math.round(((caster.agility * Calculations.DamageRatings.D) / 2) + this.damage);
	}
}

Effects.trauma = {
	name: 'Trauma',
	description: "Cracking of bones, crushing of blows, brute force can drive one's will to live.",
	color: "",
	type: Types.effectApplyTypes.DAMAGE,
	damage: 7,
	apply: function (caster, target) {
		target.health -= Math.round(((caster.strength * Calculations.DamageRatings.E) / 2) + this.damage);
	}
}

export default Effects
