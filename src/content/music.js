var Music = {};
Music.bossMusic = game.add.audio('boss_music');
Music.bossMusic.addMarker('phase1', 0, 136.241, 1, true);
Music.bossMusic.addMarker('bridge', 262.683, 1.138, 1, false);
Music.bossMusic.addMarker('phase2', 263.889, 284.870, 1, true); // repeat at 548.759s

Music.bossMusic.prototype.startPlayback = function() {
  this.playPhase(1);
}

Music.bossMusic.prototype.phaseChange = function(phase) { //changes music to next phase of bossfight //phase parameter must be integer of phase number starting at 1
  this.fadeOut(750);
  this.onStop.add(this.play('bridge'));
  this.onMarkerComplete.add(this.playPhase(phase));
}

Music.bossMusic.prototype.playPhase = function(phase) {
  var phaseKey = 'phase' + phase;
  this.play(phase);
}

export default Music
