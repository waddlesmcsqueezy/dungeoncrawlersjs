import Types from './Types'
import Calculations from './Calculations'

var Attacks = {}

//    TEMPLATE
// Attacks. = {
//     name :"",
//     use : function(caster,target) {}
// }

//MELEE

Attacks.melee.slash = {
  name: "Slash", // name of attack
  type: Types.PHYSICAL, // damage type
  damage: Calculations.DamageRatings.F, // damage multiplier, F to S.
  accuracyMod: 1, //accuracy modifier is a floating point value from 0.01 to 1.00; 0.01 being 1%, 1.00 being 100% chance to hit base
  rangeMod: 0, // range modifier; measured in tiles
  moveCost: 1, // move cost; how many moves this uses
  comboMod: 0, // combo attack multiplier; how much damage this attack does if it is used after an attack
  spriteKey: 'error', // key for the sprite that represents this attack action
  use: function () {

  }
}

Attacks.melee.thrust = {
  name: "Thrust",
  type: Types.PIERCING,
  damage: Calculations.DamageRatings.E,
  accuracyMod: 0.98,
  rangeMod: 1,
  moveCost: 1,
  comboMod: 0,
  spriteKey: 'error',
  use: function () {

  }
}

Attacks.melee.uppercut = {
  name: "Uppercut",
  type: Types.PHYSICAL,
  damage: Calculations.DamageRatings.D,
  accuracyMod: 0.97,
  rangeMod: 0,
  moveCost: 2,
  comboMod: 0,
  spriteKey: 'error',
  use: function () {

  }
}

Attacks.melee.slam = {
  name: "Slam",
  types: Types.PHYSICAL,
  damage: Calculations.DamageRatings.D,
  accuracyMod: 1,
  rangeMod: 0,
  moveCost: 3,
  comboMod: 0,
  spriteKey: 'error',
  use: function () {

  }
}

Attacks.melee.powerSwing = {
  name: "Power Slash",
  types: Types.PHYSICAL,
  damage: Calculations.DamageRatings.E,
  accuracyMod: 0.96,
  rangeMod: 0,
  moveCost: 2,
  comboMod: 0,
  spriteKey: 'error',
  use: function () {

  }
}

//RANGED

Attacks.ranged.shot = {
    name: "Shot",
}

Attacks.ranged.pierce = {
    name: "Piercing Shot",
}

Attacks.ranged.shrapnel = {
    name: "Shrapnel",
}

//MAGIC

Attacks.magic.sorceryGreatsword = {
    name: "Cast Sunsword",
}

Attacks.magic.sorceryBeam = {
    name: "Cast Sunbeam",
}

Attacks.magic.sorceryArmorBuff1 = {
    name: "Cast Sun Armor",
}

export default Attacks
