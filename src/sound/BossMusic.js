export default class BossMusic {
  constructor(game, phaseCount, key, volume, loop, connect) {
    this.audio = game.add.sound(key, volume, loop, connect);
    console.log(this.audio, game);
    this.currentPhase = 1;
  }

  startPlayback() {
    this.phaseKey = 'phase';
    this.phaseKey = this.phaseKey.concat(1);
    this.playPhase(this.phaseKey);
  }

  phaseChange(phase) { //changes music to next phase of bossfight //phase parameter must be integer of phase number starting at 1
    this.phaseKey = 'phase';
    this.phaseKey = this.phaseKey.concat(phase); //change phase to next
    this.audio.fadeOut(500);
    this.audio.onStop.add(this.playPhase, this);
  }

  playBridge() {
    this.audio.play('bridge', 0, 0.2);
    if (this.audio.currentMarker = 'bridge') {
      this.audio.onStop.add(this.playPhase, this);
    }
  }

  playPhase() {
    console.log(this.audio);
    this.audio.play(this.phaseKey, 0, 0.2);
  }
}
