export default class GuiGroup extends Phaser.Group {
  constructor(game) {
    super(game);
    game.add.existing(this);
  }
}

GuiGroup.prototype.setAlpha = function(alpha) {
  this.alpha = alpha;
}
