import BaseGui from './BaseGui';
import Dropdown from './Dropdown';
import Types from '../content/Types';

export default class InventoryGui extends BaseGui {
  constructor(game) {
    super(game, 0, 0, '16_9_bg_inv', "Inventory");
    this.slotCount = {x: 1, y: 10};
    this.offsetMode = "PIXEL" // MODES (USE STRINGS TO IDENTIFY MODES): | PIXEL - MEASURED IN PIXELS | PERCENT - MEASRUED IN FLOATING POINT VALUE FROM 0 TO 1 | DONT USE "PIXEL" RIGHT NOW
    this.slotOffsetX = 64; // measured in PX
    this.slotOffsetY = 64;
  }
}

// InventoryGui.prototype.toggle = function(game) {
//   this.bringToTop();
//   this.title.bringToTop();
//   if (this.visible == false) {
//     this.open(game);
//   } else if (this.visible == true) {
//     this.close(game);
//   }
// }

InventoryGui.prototype.open = function(game) {
  console.log("open");
  this.visible = true;
  this.title.visible = true;
  this.updateItemList(game);
}

InventoryGui.prototype.close = function(game) {
  console.log("close");
  this.resetItemList();
  this.visible = false;
  this.title.visible = false;
}

InventoryGui.prototype.newDropdown = function(game) {
  var dropdown = new Dropdown(game);
  return dropdown;
}

InventoryGui.prototype.placeSprite = function(game) {
  for(i = 0; this.game.world.guiGroup[2].itemList.length > i; i++) {
    if (this.sprite.x == this.game.world.guiGroup[2].itemList[i].x) {
      console.log("x match")
    }
  }
}

InventoryGui.prototype.actionOnClick = function(game) {
  var locations = {
    INVENTORY: 0,
    LEFTHAND: 1,
    RIGHTHAND: 2,
    HEAD: 3,
    TORSO: 4,
    LEG: 5,
    AMMO: 6
  }

  if (this.game.world.actors[0].inventory.getItemById(this.data.itemKey) != null) {
    var item = [this.data.itemKey, this.game.world.actors[0].inventory.getItemById(this.data.itemKey)]
    var location = locations.INVENTORY;
  } else if (this.game.world.actors[0].equippedItems.leftHand.getItem() != null) {
    if (this.game.world.actors[0].equippedItems.leftHand.getItem()[0] == this.data.itemKey) {
      var item = this.game.world.actors[0].equippedItems.leftHand.getItem();
      var location = locations.LEFTHAND;
    }
  } else if (this.game.world.actors[0].equippedItems.rightHand.getItem() != null) {
    if (this.game.world.actors[0].equippedItems.rightHand.getItem()[0] == this.data.itemKey) {
      var item = this.game.world.actors[0].equippedItems.rightHand.getItem();
      var location = locations.RIGHTHAND;
    }
  } else if (this.game.world.actors[0].equippedItems.head.getItem() != null) {
    if (this.game.world.actors[0].equippedItems.head.getItem()[0] == this.data.itemKey) {
      var item = this.game.world.actors[0].equippedItems.head.getItem();
      var location = locations.HEAD;
    }
  } else if (this.game.world.actors[0].equippedItems.torso.getItem() != null) {
    if (this.game.world.actors[0].equippedItems.torso.getItem()[0] == this.data.itemKey) {
      var item = this.game.world.actors[0].equippedItems.torso.getItem();
      var location = locations.TORSO;
    }
  } else if (this.game.world.actors[0].equippedItems.leg.getItem() != null) {
    if (this.game.world.actors[0].equippedItems.leg.getItem()[0] == this.data.itemKey) {
      var item = this.game.world.actors[0].equippedItems.leg.getItem();
      var location = locations.LEG;
    }
  } else if (this.game.world.actors[0].equippedItems.ammo.getItem() != null) {
    if (this.game.world.actors[0].equippedItems.ammo.getItem()[0] == this.data.itemKey) {
      var item = this.game.world.actors[0].equippedItems.ammo.getItem();
      var location = locations.AMMO;
    }
  }
  console.log(this.game.world.actors[0].equippedItems.rightHand.getItem())
  console.log(item);
  console.log(this.data.itemKey);
  // console.log(location);

  if (this.game.input.activePointer.leftButton.shiftKey) { // IF THE SHIFT KEY WAS PRESSED THE LAST TIME THE LEFT BUTTON WAS CLICKED
    switch (true) {
      case location == locations.INVENTORY && item[1].type == Types.itemTypes.WEAPON:
        if (this.game.world.actors[0].equippedItems.leftHand.getItem() != null) {
          this.game.world.actors[0].equippedItems.leftHand.swapItem(this.game, this.game.world.actors[0].inventory, item);
        } else { this.game.world.actors[0].equippedItems.leftHand.equipItem(this.game, item, this.game.world.actors[0].inventory); }
        break;

      case location == locations.INVENTORY && item[1].type == Types.itemTypes.HEAD:
        if (this.game.world.actors[0].equippedItems.head.getItem() != null) {
          this.game.world.actors[0].equippedItems.head.swapItem(this.game, this.game.world.actors[0].inventory, item);
        } else { this.game.world.actors[0].equippedItems.head.equipItem(this.game, item, this.game.world.actors[0].inventory); }
        break;
      case location == locations.HEAD && item[1].type == Types.itemTypes.HEAD:
        this.game.world.actors[0].equippedItems.head.transferItemToContainer(this.game, this.game.world.actors[0].inventory);
        break;

      case location == locations.INVENTORY && item[1].type == Types.itemTypes.TORSO:
        if (this.game.world.actors[0].equippedItems.torso.getItem() != null) {
          this.game.world.actors[0].equippedItems.torso.swapItem(this.game, this.game.world.actors[0].inventory, item);
        } else { this.game.world.actors[0].equippedItems.torso.equipItem(this.game, item, this.game.world.actors[0].inventory); }
        break;
      case location == locations.TORSO && item[1].type == Types.itemTypes.TORSO:
        this.game.world.actors[0].equippedItems.torso.transferItemToContainer(this.game, this.game.world.actors[0].inventory);
        break;

      case location == locations.INVENTORY && item[1].type == Types.itemTypes.LEG:
        if (this.game.world.actors[0].equippedItems.leg.getItem() != null) {
          this.game.world.actors[0].equippedItems.leg.swapItem(this.game, this.game.world.actors[0].inventory, item);
        } else { this.game.world.actors[0].equippedItems.leg.equipItem(this.game, item, this.game.world.actors[0].inventory); }
        break;
      case location == locations.LEG && item[1].type == Types.itemTypes.LEG:
        this.game.world.actors[0].equippedItems.leg.transferItemToContainer(this.game, this.game.world.actors[0].inventory);
        break;

      case location == locations.INVENTORY && item[1].type == Types.itemTypes.AMMO:
        if (this.game.world.actors[0].equippedItems.ammo.getItem() != null) {
          this.game.world.actors[0].equippedItems.ammo.swapItem(this.game, this.game.world.actors[0].inventory, item);
        } else { this.game.world.actors[0].equippedItems.ammo.equipItem(this.game, item, this.game.world.actors[0].inventory); }
        break;
      case location == locations.AMMO && item[1].type == Types.itemTypes.AMMO:
        this.game.world.actors[0].equippedItems.ammo.transferItemToContainer(this.game, this.game.world.actors[0].inventory);
        break;

      case location == locations.LEFTHAND && item[1].type == Types.itemTypes.WEAPON:
        this.game.world.actors[0].equippedItems.leftHand.transferItemToContainer(this.game, this.game.world.actors[0].inventory);
        break;
      case location == locations.RIGHTHAND && item[1].type == Types.itemTypes.WEAPON:
        console.log(item)
        this.game.world.actors[0].equippedItems.rightHand.transferItemToSlot(this.game, this.game.world.actors[0].equippedItems.leftHand);
        break;
    }
  } else {                                  // IF WE LEFT CLICK
    switch (true) {
      case location == locations.INVENTORY && item[1].type == Types.itemTypes.WEAPON:
        if (this.game.world.actors[0].equippedItems.rightHand.getItem() != null) {
          this.game.world.actors[0].equippedItems.rightHand.swapItem(this.game, this.game.world.actors[0].inventory, item);
        } else { this.game.world.actors[0].equippedItems.rightHand.equipItem(this.game, item, this.game.world.actors[0].inventory); }
        break;

      case location == locations.INVENTORY && item[1].type == Types.itemTypes.HEAD:
        if (this.game.world.actors[0].equippedItems.head.getItem() != null) {
          this.game.world.actors[0].equippedItems.head.swapItem(this.game, this.game.world.actors[0].inventory, item);
        } else { this.game.world.actors[0].equippedItems.head.equipItem(this.game, item, this.game.world.actors[0].inventory); }
        break;
      case location == locations.HEAD && item[1].type == Types.itemTypes.HEAD:
        this.game.world.actors[0].equippedItems.head.transferItemToContainer(this.game, this.game.world.actors[0].inventory);
        break;

      case location == locations.INVENTORY && item[1].type == Types.itemTypes.TORSO:
        if (this.game.world.actors[0].equippedItems.torso.getItem() != null) {
          this.game.world.actors[0].equippedItems.torso.swapItem(this.game, this.game.world.actors[0].inventory, item);
          console.log("swapping item")
        } else { this.game.world.actors[0].equippedItems.torso.equipItem(this.game, item, this.game.world.actors[0].inventory); console.log("equipping item") }
        break;
      case location == locations.TORSO && item[1].type == Types.itemTypes.TORSO:
        this.game.world.actors[0].equippedItems.torso.transferItemToContainer(this.game, this.game.world.actors[0].inventory);
        console.log("transfering item")
        break;

      case location == locations.INVENTORY && item[1].type == Types.itemTypes.LEG:
        if (this.game.world.actors[0].equippedItems.leg.getItem() != null) {
          this.game.world.actors[0].equippedItems.leg.swapItem(this.game, this.game.world.actors[0].inventory, item);
        } else { this.game.world.actors[0].equippedItems.leg.equipItem(this.game, item, this.game.world.actors[0].inventory); }
        break;
      case location == locations.LEG && item[1].type == Types.itemTypes.LEG:
        this.game.world.actors[0].equippedItems.leg.transferItemToContainer(this.game, this.game.world.actors[0].inventory);
        break;

      case location == locations.INVENTORY && item[1].type == Types.itemTypes.AMMO:
        if (this.game.world.actors[0].equippedItems.ammo.getItem() != null) {
          this.game.world.actors[0].equippedItems.ammo.swapItem(this.game, this.game.world.actors[0].inventory, item);
        } else { this.game.world.actors[0].equippedItems.ammo.equipItem(this.game, item, this.game.world.actors[0].inventory); }
        break;
      case location == locations.AMMO && item[1].type == Types.itemTypes.AMMO:
        this.game.world.actors[0].equippedItems.ammo.transferItemToContainer(this.game, this.game.world.actors[0].inventory);
        break;

      case location == locations.LEFTHAND && item[1].type == Types.itemTypes.WEAPON:
        this.game.world.actors[0].equippedItems.leftHand.transferItemToSlot(this.game, this.game.world.actors[0].equippedItems.rightHand);
        break;
      case location == locations.RIGHTHAND && item[1].type == Types.itemTypes.WEAPON:
        console.log("treansfering item")
        this.game.world.actors[0].equippedItems.rightHand.transferItemToContainer(this.game, this.game.world.actors[0].inventory);
        break;
    }
  }
}

InventoryGui.prototype.resetItemList = function() {
  if (this.itemList != null) {
    for(var i = 0; i < this.itemList.length; i++) {
      // this.itemList[i].shadow.destroy(true);
      if (this.itemList[i].text != null) {
        this.itemList[i].text.destroy(true);
      }
      if (this.itemList[i].equippedSprite) {
        this.itemList[i].equippedSprite.destroy(true);
      }
      this.itemList[i].destroy(true);
    }
    this.itemList = null;
  } else { console.log("itemList doesn't exist"); }
  // console.log(this.itemList);
}

InventoryGui.prototype.updateItemList = function(game) {
  this.resetItemList();
  var invCount = 0;
  var equipUI = {
    leftHand: false,
    rightHand: false,
    head: false,
    torso: false,
    leg: false,
    ammo: false
  }
  this.itemList = [];
  if (this.visible == true) {
    for(var i = 1; i <= this.slotCount.x * this.slotCount.y; i++) { //game.world.actors[0].inventory.getItems().length
      var column = Math.floor((i-1) / this.slotCount.x); // determines the row of i
      var row = Math.floor((i-1) / this.slotCount.y); // determines the column of i
      var rowPos = (row);
      var columnPos = i-(column * this.slotCount.x);
      var xPos = columnPos / this.slotCount.x;
      var yPos = (column / this.slotCount.y) * 0.9;

      this.itemList[i-1] = game.add.button(0, 0, 'selection_slim_gui', this.actionOnClick, this.itemList[i], 2, 1, 0); //we use getItems()[i][1] because [0] contains the values of the item, 0 contains the key.
      this.itemList[i-1].scale.setTo(this.width / (this.slotCount.x + 2) / this.itemList[i-1].width,
      this.width / (this.slotCount.y * 2.5) / this.itemList[i-1].height);
      this.itemList[i-1].fixedToCamera = true;
      this.itemList[i-1].smoothed = false;
      this.itemList[i-1].anchor.setTo(0.5);
      this.itemList[i-1].bringToTop();
      this.itemList[i-1].cameraOffset.setTo(((xPos * this.width) - (this.itemList[i-1].width * 2.3) + this.cameraOffset.x),
      ((this.itemList[i-1].height * 1.2) + (yPos * this.height)));

      if (game.world.actors[0].equippedItems.leftHand.getItem() != null && !equipUI.leftHand) {

        this.itemList[i-1].data.itemKey = game.world.actors[0].equippedItems.leftHand.getItem()[0];
        this.itemList[i-1].text = game.add.text(0, 0, game.world.actors[0].equippedItems.leftHand.getItem()[1].name); //we use getItems()[i][1] because [0] contains the values of the item
        // this.itemList[i-1].addChild(this.itemList[i-1].text);
        this.itemList[i-1].text.font = 'Press Start 2P';
        this.itemList[i-1].text.setShadow(2, 2, 'rgba(0,0,0,0.5)', 5);
        this.itemList[i-1].text.fixedToCamera = true;
        this.itemList[i-1].text.scale.setTo(0.8, 0.8);
        this.itemList[i-1].text.smoothed = false;
        this.itemList[i-1].text.anchor.setTo(0, 0.5);
        this.itemList[i-1].text.bringToTop();
        this.itemList[i-1].text.cameraOffset.setTo(this.itemList[i-1].cameraOffset.x / 2,
        ((this.itemList[i-1].height * 1.2) + (yPos * this.height)));

        this.itemList[i-1].equippedSprite = game.add.sprite(0, 0, 'equipped_icon_left');
        this.itemList[i-1].equippedSprite.smoothed = false;
        this.itemList[i-1].equippedSprite.fixedToCamera = true;
        this.itemList[i-1].equippedSprite.anchor.setTo(0, 0.5);
        this.itemList[i-1].equippedSprite.bringToTop();
        this.itemList[i-1].equippedSprite.cameraOffset.setTo(this.itemList[i-1].cameraOffset.x / 3,
        ((this.itemList[i-1].height * 1.2) + (yPos * this.height)));
        equipUI.leftHand = true;

      } else if (game.world.actors[0].equippedItems.rightHand.getItem() != null && !equipUI.rightHand) {

        this.itemList[i-1].data.itemKey = game.world.actors[0].equippedItems.rightHand.getItem()[0];
        this.itemList[i-1].text = game.add.text(0, 0, game.world.actors[0].equippedItems.rightHand.getItem()[1].name); //we use getItems()[i][1] because [0] contains the values of the item
        // this.itemList[i-1].addChild(this.itemList[i-1].text);
        this.itemList[i-1].text.font = 'Press Start 2P';
        this.itemList[i-1].text.setShadow(2, 2, 'rgba(0,0,0,0.5)', 5);
        this.itemList[i-1].text.fixedToCamera = true;
        this.itemList[i-1].text.scale.setTo(0.8, 0.8);
        this.itemList[i-1].text.smoothed = false;
        this.itemList[i-1].text.anchor.setTo(0, 0.5);
        this.itemList[i-1].text.bringToTop();
        this.itemList[i-1].text.cameraOffset.setTo(this.itemList[i-1].cameraOffset.x / 2,
        ((this.itemList[i-1].height * 1.2) + (yPos * this.height)));

        this.itemList[i-1].equippedSprite = game.add.sprite(0, 0, 'equipped_icon_right');
        this.itemList[i-1].equippedSprite.smoothed = false;
        this.itemList[i-1].equippedSprite.fixedToCamera = true;
        this.itemList[i-1].equippedSprite.anchor.setTo(0, 0.5);
        this.itemList[i-1].equippedSprite.bringToTop();
        this.itemList[i-1].equippedSprite.cameraOffset.setTo(this.itemList[i-1].cameraOffset.x / 3,
        ((this.itemList[i-1].height * 1.2) + (yPos * this.height)));
        equipUI.rightHand = true;

      } else if (game.world.actors[0].equippedItems.head.getItem() != null && !equipUI.head) {

        this.itemList[i-1].data.itemKey = game.world.actors[0].equippedItems.head.getItem()[0];
        this.itemList[i-1].text = game.add.text(0, 0, game.world.actors[0].equippedItems.head.getItem()[1].name); //we use getItems()[i][1] because [0] contains the values of the item
        // this.itemList[i-1].addChild(this.itemList[i-1].text);
        this.itemList[i-1].text.font = 'Press Start 2P';
        this.itemList[i-1].text.setShadow(2, 2, 'rgba(0,0,0,0.5)', 5);
        this.itemList[i-1].text.fixedToCamera = true;
        this.itemList[i-1].text.scale.setTo(0.8, 0.8);
        this.itemList[i-1].text.smoothed = false;
        this.itemList[i-1].text.anchor.setTo(0, 0.5);
        this.itemList[i-1].text.bringToTop();
        this.itemList[i-1].text.cameraOffset.setTo(this.itemList[i-1].cameraOffset.x / 2,
        ((this.itemList[i-1].height * 1.2) + (yPos * this.height)));

        this.itemList[i-1].equippedSprite = game.add.sprite(0, 0, 'equipped_icon');
        this.itemList[i-1].equippedSprite.smoothed = false;
        this.itemList[i-1].equippedSprite.fixedToCamera = true;
        this.itemList[i-1].equippedSprite.anchor.setTo(0, 0.5);
        this.itemList[i-1].equippedSprite.bringToTop();
        this.itemList[i-1].equippedSprite.cameraOffset.setTo(this.itemList[i-1].cameraOffset.x / 3,
        ((this.itemList[i-1].height * 1.2) + (yPos * this.height)));
        equipUI.head = true;

      } else if (game.world.actors[0].equippedItems.torso.getItem() != null && !equipUI.torso) {

        this.itemList[i-1].data.itemKey = game.world.actors[0].equippedItems.torso.getItem()[0];
        this.itemList[i-1].text = game.add.text(0, 0, game.world.actors[0].equippedItems.torso.getItem()[1].name); //we use getItems()[i][1] because [0] contains the values of the item
        // this.itemList[i-1].addChild(this.itemList[i-1].text);
        this.itemList[i-1].text.font = 'Press Start 2P';
        this.itemList[i-1].text.setShadow(2, 2, 'rgba(0,0,0,0.5)', 5);
        this.itemList[i-1].text.fixedToCamera = true;
        this.itemList[i-1].text.scale.setTo(0.8, 0.8);
        this.itemList[i-1].text.smoothed = false;
        this.itemList[i-1].text.anchor.setTo(0, 0.5);
        this.itemList[i-1].text.bringToTop();
        this.itemList[i-1].text.cameraOffset.setTo(this.itemList[i-1].cameraOffset.x / 2,
        ((this.itemList[i-1].height * 1.2) + (yPos * this.height)));

        this.itemList[i-1].equippedSprite = game.add.sprite(0, 0, 'equipped_icon');
        this.itemList[i-1].equippedSprite.smoothed = false;
        this.itemList[i-1].equippedSprite.fixedToCamera = true;
        this.itemList[i-1].equippedSprite.anchor.setTo(0, 0.5);
        this.itemList[i-1].equippedSprite.bringToTop();
        this.itemList[i-1].equippedSprite.cameraOffset.setTo(this.itemList[i-1].cameraOffset.x / 3,
        ((this.itemList[i-1].height * 1.2) + (yPos * this.height)));
        equipUI.torso = true;

      } else if (game.world.actors[0].equippedItems.leg.getItem() != null && !equipUI.leg) {

        this.itemList[i-1].data.itemKey = game.world.actors[0].equippedItems.leg.getItem()[0];
        this.itemList[i-1].text = game.add.text(0, 0, game.world.actors[0].equippedItems.leg.getItem()[1].name); //we use getItems()[i][1] because [0] contains the values of the item
        // this.itemList[i-1].addChild(this.itemList[i-1].text);
        this.itemList[i-1].text.font = 'Press Start 2P';
        this.itemList[i-1].text.setShadow(2, 2, 'rgba(0,0,0,0.5)', 5);
        this.itemList[i-1].text.fixedToCamera = true;
        this.itemList[i-1].text.scale.setTo(0.8, 0.8);
        this.itemList[i-1].text.smoothed = false;
        this.itemList[i-1].text.anchor.setTo(0, 0.5);
        this.itemList[i-1].text.bringToTop();
        this.itemList[i-1].text.cameraOffset.setTo(this.itemList[i-1].cameraOffset.x / 2,
        ((this.itemList[i-1].height * 1.2) + (yPos * this.height)));

        this.itemList[i-1].equippedSprite = game.add.sprite(0, 0, 'equipped_icon');
        this.itemList[i-1].equippedSprite.smoothed = false;
        this.itemList[i-1].equippedSprite.fixedToCamera = true;
        this.itemList[i-1].equippedSprite.anchor.setTo(0, 0.5);
        this.itemList[i-1].equippedSprite.bringToTop();
        this.itemList[i-1].equippedSprite.cameraOffset.setTo(this.itemList[i-1].cameraOffset.x / 3,
        ((this.itemList[i-1].height * 1.2) + (yPos * this.height)));
        equipUI.leg = true;

      } else if (game.world.actors[0].equippedItems.ammo.getItem() != null && !equipUI.ammo) {

        this.itemList[i-1].data.itemKey = game.world.actors[0].equippedItems.ammo.getItem()[0];
        this.itemList[i-1].text = game.add.text(0, 0, game.world.actors[0].equippedItems.ammo.getItem()[1].name); //we use getItems()[i][1] because [0] contains the values of the item
        // this.itemList[i-1].addChild(this.itemList[i-1].text);
        this.itemList[i-1].text.font = 'Press Start 2P';
        this.itemList[i-1].text.setShadow(2, 2, 'rgba(0,0,0,0.5)', 5);
        this.itemList[i-1].text.fixedToCamera = true;
        this.itemList[i-1].text.scale.setTo(0.8, 0.8);
        this.itemList[i-1].text.smoothed = false;
        this.itemList[i-1].text.anchor.setTo(0, 0.5);
        this.itemList[i-1].text.bringToTop();
        this.itemList[i-1].text.cameraOffset.setTo(this.itemList[i-1].cameraOffset.x / 2,
        ((this.itemList[i-1].height * 1.2) + (yPos * this.height)));

        this.itemList[i-1].equippedSprite = game.add.sprite(0, 0, 'equipped_icon');
        this.itemList[i-1].equippedSprite.smoothed = false;
        this.itemList[i-1].equippedSprite.fixedToCamera = true;
        this.itemList[i-1].equippedSprite.anchor.setTo(0, 0.5);
        this.itemList[i-1].equippedSprite.bringToTop();
        this.itemList[i-1].equippedSprite.cameraOffset.setTo(this.itemList[i-1].cameraOffset.x / 3,
        ((this.itemList[i-1].height * 1.2) + (yPos * this.height)));
        equipUI.ammo = true;

      } else if (game.world.actors[0].inventory.getItems()[invCount] != null) {

        this.itemList[i-1].data.itemKey = game.world.actors[0].inventory.getItems()[invCount][0];
        this.itemList[i-1].text =
        game.add.text(0, 0, game.world.actors[0].inventory.getItems()[invCount][1].name); //we use getItems()[i][1] because [0] contains the values of the item
        // this.itemList[i-1].addChild(this.itemList[i-1].text);
        this.itemList[i-1].text.font = 'Press Start 2P';
        this.itemList[i-1].text.setShadow(2, 2, 'rgba(0,0,0,0.5)', 5);
        this.itemList[i-1].text.fixedToCamera = true;
        this.itemList[i-1].text.scale.setTo(0.8, 0.8);
        this.itemList[i-1].text.smoothed = false;
        this.itemList[i-1].text.anchor.setTo(0, 0.5);
        this.itemList[i-1].text.bringToTop();
        this.itemList[i-1].text.cameraOffset.setTo(this.itemList[i-1].cameraOffset.x / 2,
        ((this.itemList[i-1].height * 1.2) + (yPos * this.height)));
        invCount++;
      }
    }
  }
}
