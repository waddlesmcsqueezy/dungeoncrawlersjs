import BaseGui from './BaseGui'

export default class EquipmentGui extends BaseGui {
  constructor(game) {
    super(game, 0, 0, 'square_icon', "Equipment");
    this.slotCount = {x: 3, y: 3};
  }
}

// InventoryGui.prototype.toggle = function(game) {
//   this.bringToTop();
//   this.title.bringToTop();
//   if (this.visible == false) {
//     this.open(game);
//   } else if (this.visible == true) {
//     this.close(game);
//   }
// }
EquipmentGui.prototype.setup = function(game) {
  this.scale.setTo((document.documentElement.clientWidth / game.world.guiGroup[0].width + game.world.guiGroup[1].width) / this.width,
  (document.documentElement.clientWidth / game.world.guiGroup[0].width + game.world.guiGroup[1].width) / this.width);
  this.cameraOffset.setTo(game.world.guiGroup[0].x + game.world.guiGroup[0].width, 0);
  this.title.cameraOffset.setTo(this.width/2 + this.cameraOffset.x, this.title.height);
}

EquipmentGui.prototype.open = function(game) {
  console.log("open");
  this.visible = true;
  this.title.visible = true;
  this.updateItemList(game);
}

EquipmentGui.prototype.close = function(game) {
  console.log("close");
  this.resetItemList();
  this.visible = false;
  this.title.visible = false;
}

EquipmentGui.prototype.placeSprite = function(game) {
  for(i = 0; this.game.world.guiGroup[2].itemList.length > i; i++) {
    if (this.sprite.x == this.game.world.guiGroup[2].itemList[i].x) {
      console.log("x match")
    }
  }
}

EquipmentGui.prototype.actionOnClick = function(game) {
  var item = [this.itemKey, this.game.world.actors[0].inventory.getItemById(this.itemKey)]
  console.log(item);
  this.sprite.inputEnabled = true;
  this.sprite.input.enableDrag();
  this.sprite.events.onDragStop.add(placeSprite, this);
}

EquipmentGui.prototype.resetItemList = function() {
  if (this.itemList != null) {
    for(var i = 0; i < this.itemList.length; i++) {
      // this.itemList[i].shadow.destroy(true);
      if (this.itemList[i].sprite != null) {
        this.itemList[i].sprite.destroy(true);
      }
      this.itemList[i].destroy(true);
    }
    this.itemList = null;
  } else { console.log("itemList doesn't exist"); }
  console.log(this.itemList);
}

EquipmentGui.prototype.updateItemList = function(game) {
  this.resetItemList();

  this.itemList = [];
  if (this.visible == true) {
    for(var i = 1; i <= this.slotCount.x * this.slotCount.y; i++) { //game.world.actors[0].inventory.getItems().length
      var column = Math.floor((i-1)/this.slotCount.x); // determines the row of i
      var row = Math.floor((i-1)/this.slotCount.y); // determines the column of i
      var rowPos = (row);
      var columnPos = i-(column * this.slotCount.x);
      var xPos = columnPos / this.slotCount.x;
      var yPos = (column / this.slotCount.y) * 0.9;
      // console.log("i: " + i + " | " + "Row: " + row + " | " + "Column: " + column + " | " +
      // "RowPos: " + rowPos + " | " + "ColumnPos: " + columnPos + " | " + "xPos: " + xPos + " | " + "yPos: " + yPos + " | " + "x: " + xPos*this.width + " | " + "y: " + yPos*this.height);
      this.itemList[i-1] = game.add.button(0, 0, 'selection_gui', this.actionOnClick, this.itemList[i], 2, 1, 0); //we use getItems()[i][1] because [0] contains the values of the item, 0 contains the key.
      this.itemList[i-1].scale.setTo(this.width / (this.slotCount.x + 3.5) / this.itemList[i-1].width, this.width / (this.slotCount.x + 3.5) / this.itemList[i-1].width);
      this.itemList[i-1].fixedToCamera = true;
      this.itemList[i-1].smoothed = false;
      this.itemList[i-1].anchor.setTo(0.5);
      this.itemList[i-1].bringToTop();
      this.itemList[i-1].cameraOffset.setTo((xPos*this.width) - (this.itemList[i-1].width * 1) + this.x, (this.itemList[i-1].width * 1.5) + (yPos*this.height));
      // console.log(this.itemList[i].cameraOffset.y)
      if (i == 1) {
        if (game.world.actors[0].equippedItems.leftHand.getItem() != null) {
          this.itemList[i-1].itemKey = game.world.actors[0].equippedItems.leftHand.getItem()[0];
          this.itemList[i-1].sprite = game.add.sprite(0, 0, game.world.actors[0].equippedItems.leftHand.getItem()[1].spriteKey); //we use getItems()[i][1] because [0] contains the values of the item
          this.itemList[i-1].sprite.fixedToCamera = true;
          this.itemList[i-1].sprite.scale.setTo(this.width / (this.slotCount.x + 3.5) / this.itemList[i-1].sprite.width, this.width / (this.slotCount.x + 3.5) / this.itemList[i-1].sprite.width);
          this.itemList[i-1].sprite.smoothed = false;
          this.itemList[i-1].sprite.anchor.setTo(0.5);
          this.itemList[i-1].sprite.bringToTop();
          this.itemList[i-1].sprite.cameraOffset.setTo((xPos*this.width) - (this.itemList[i-1].sprite.width * 1) + this.x,
          (this.itemList[i-1].sprite.width * 1.5) + (yPos*this.height));
        }
      } else if (i == 2) {
        if (game.world.actors[0].equippedItems.head.getItem() != null) {
          this.itemList[i-1].itemKey = game.world.actors[0].equippedItems.head.getItem()[0];
          this.itemList[i-1].sprite = game.add.sprite(0, 0, game.world.actors[0].equippedItems.head.getItem()[1].spriteKey); //we use getItems()[i][1] because [0] contains the values of the item
          this.itemList[i-1].sprite.fixedToCamera = true;
          this.itemList[i-1].sprite.scale.setTo(this.width / (this.slotCount.x + 3.5) / this.itemList[i-1].sprite.width, this.width / (this.slotCount.x + 3.5) / this.itemList[i-1].sprite.width);
          this.itemList[i-1].sprite.smoothed = false;
          this.itemList[i-1].sprite.anchor.setTo(0.5);
          this.itemList[i-1].sprite.bringToTop();
          this.itemList[i-1].sprite.cameraOffset.setTo((xPos*this.width) - (this.itemList[i-1].sprite.width * 1) + this.x,
          (this.itemList[i-1].sprite.width * 1.5) + (yPos*this.height));
        }
      } else if (i == 3) {
        if (game.world.actors[0].equippedItems.rightHand.getItem() != null) {
          this.itemList[i-1].itemKey = game.world.actors[0].equippedItems.rightHand.getItem()[0];
          this.itemList[i-1].sprite = game.add.sprite(0, 0, game.world.actors[0].equippedItems.rightHand.getItem()[1].spriteKey); //we use getItems()[i][1] because [0] contains the values of the item
          this.itemList[i-1].sprite.fixedToCamera = true;
          this.itemList[i-1].sprite.scale.setTo(this.width / (this.slotCount.x + 3.5) / this.itemList[i-1].sprite.width, this.width / (this.slotCount.x + 3.5) / this.itemList[i-1].sprite.width);
          this.itemList[i-1].sprite.smoothed = false;
          this.itemList[i-1].sprite.anchor.setTo(0.5);
          this.itemList[i-1].sprite.bringToTop();
          this.itemList[i-1].sprite.cameraOffset.setTo((xPos*this.width) - (this.itemList[i-1].sprite.width * 1) + this.x,
          (this.itemList[i-1].sprite.width * 1.5) + (yPos*this.height));
        }
      } else if (i == 4) {
        if (game.world.actors[0].equippedItems.leftAmmo1.getItem() != null) {
          this.itemList[i-1].itemKey = game.world.actors[0].equippedItems.leftAmmo1.getItem()[0];
          this.itemList[i-1].sprite = game.add.sprite(0, 0, game.world.actors[0].equippedItems.leftAmmo1.getItem()[1].spriteKey); //we use getItems()[i][1] because [0] contains the values of the item
          this.itemList[i-1].sprite.fixedToCamera = true;
          this.itemList[i-1].sprite.scale.setTo(this.width / (this.slotCount.x + 3.5) / this.itemList[i-1].sprite.width, this.width / (this.slotCount.x + 3.5) / this.itemList[i-1].sprite.width);
          this.itemList[i-1].sprite.smoothed = false;
          this.itemList[i-1].sprite.anchor.setTo(0.5);
          this.itemList[i-1].sprite.bringToTop();
          this.itemList[i-1].sprite.cameraOffset.setTo((xPos*this.width) - (this.itemList[i-1].sprite.width * 1) + this.x,
          (this.itemList[i-1].sprite.width * 1.5) + (yPos*this.height));
        }
      } else if (i == 5) {
        if (game.world.actors[0].equippedItems.torso.getItem() != null) {
          this.itemList[i-1].itemKey = game.world.actors[0].equippedItems.torso.getItem()[0];
          this.itemList[i-1].sprite = game.add.sprite(0, 0, game.world.actors[0].equippedItems.leg.getItem()[1].spriteKey); //we use getItems()[i][1] because [0] contains the values of the item
          this.itemList[i-1].sprite.fixedToCamera = true;
          this.itemList[i-1].sprite.scale.setTo(this.width / (this.slotCount.x + 3.5) / this.itemList[i-1].sprite.width, this.width / (this.slotCount.x + 3.5) / this.itemList[i-1].sprite.width);
          this.itemList[i-1].sprite.smoothed = false;
          this.itemList[i-1].sprite.anchor.setTo(0.5);
          this.itemList[i-1].sprite.bringToTop();
          this.itemList[i-1].sprite.cameraOffset.setTo((xPos*this.width) - (this.itemList[i-1].sprite.width * 1) + this.x,
          (this.itemList[i-1].sprite.width * 1.5) + (yPos*this.height));
        }
      } else if (i == 6) {
        if (game.world.actors[0].equippedItems.rightAmmo1.getItem() != null) {
          this.itemList[i-1].itemKey = game.world.actors[0].equippedItems.rightAmmo1.getItem()[0];
          this.itemList[i-1].sprite = game.add.sprite(0, 0, game.world.actors[0].equippedItems.rightAmmo1.getItem()[1].spriteKey); //we use getItems()[i][1] because [0] contains the values of the item
          this.itemList[i-1].sprite.fixedToCamera = true;
          this.itemList[i-1].sprite.scale.setTo(this.width / (this.slotCount.x + 3.5) / this.itemList[i-1].sprite.width, this.width / (this.slotCount.x + 3.5) / this.itemList[i-1].sprite.width);
          this.itemList[i-1].sprite.smoothed = false;
          this.itemList[i-1].sprite.anchor.setTo(0.5);
          this.itemList[i-1].sprite.bringToTop();
          this.itemList[i-1].sprite.cameraOffset.setTo((xPos*this.width) - (this.itemList[i-1].sprite.width * 1) + this.x,
          (this.itemList[i-1].sprite.width * 1.5) + (yPos*this.height));
        }
      } else if (i == 7) {
        if (game.world.actors[0].equippedItems.leftAmmo2.getItem() != null) {
          this.itemList[i-1].itemKey = game.world.actors[0].equippedItems.leftAmmo2.getItem()[0];
          this.itemList[i-1].sprite = game.add.sprite(0, 0, game.world.actors[0].equippedItems.leftAmmo2.getItem()[1].spriteKey); //we use getItems()[i][1] because [0] contains the values of the item
          this.itemList[i-1].sprite.fixedToCamera = true;
          this.itemList[i-1].sprite.scale.setTo(this.width / (this.slotCount.x + 3.5) / this.itemList[i-1].sprite.width, this.width / (this.slotCount.x + 3.5) / this.itemList[i-1].sprite.width);
          this.itemList[i-1].sprite.smoothed = false;
          this.itemList[i-1].sprite.anchor.setTo(0.5);
          this.itemList[i-1].sprite.bringToTop();
          this.itemList[i-1].sprite.cameraOffset.setTo((xPos*this.width) - (this.itemList[i-1].sprite.width * 1) + this.x,
          (this.itemList[i-1].sprite.width * 1.5) + (yPos*this.height));
        }
      } else if (i == 8) {
        if (game.world.actors[0].equippedItems.leg.getItem() != null) {
          this.itemList[i-1].itemKey = game.world.actors[0].equippedItems.leg.getItem()[0];
          this.itemList[i-1].sprite = game.add.sprite(0, 0, game.world.actors[0].equippedItems.leg.getItem()[1].spriteKey); //we use getItems()[i][1] because [0] contains the values of the item
          this.itemList[i-1].sprite.fixedToCamera = true;
          this.itemList[i-1].sprite.scale.setTo(this.width / (this.slotCount.x + 3.5) / this.itemList[i-1].sprite.width, this.width / (this.slotCount.x + 3.5) / this.itemList[i-1].sprite.width);
          this.itemList[i-1].sprite.smoothed = false;
          this.itemList[i-1].sprite.anchor.setTo(0.5);
          this.itemList[i-1].sprite.bringToTop();
          this.itemList[i-1].sprite.cameraOffset.setTo((xPos*this.width) - (this.itemList[i-1].sprite.width * 1) + this.x,
          (this.itemList[i-1].sprite.width * 1.5) + (yPos*this.height));
        }
      } if (i == 9) {
        if (game.world.actors[0].equippedItems.rightAmmo2.getItem() != null) {
          this.itemList[i-1].itemKey = game.world.actors[0].equippedItems.rightAmmo2.getItem()[0];
          this.itemList[i-1].sprite = game.add.sprite(0, 0, game.world.actors[0].equippedItems.rightAmmo2.getItem()[1].spriteKey); //we use getItems()[i][1] because [0] contains the values of the item
          this.itemList[i-1].sprite.fixedToCamera = true;
          this.itemList[i-1].sprite.scale.setTo(this.width / (this.slotCount.x + 3.5) / this.itemList[i-1].sprite.width, this.width / (this.slotCount.x + 3.5) / this.itemList[i-1].sprite.width);
          this.itemList[i-1].sprite.smoothed = false;
          this.itemList[i-1].sprite.anchor.setTo(0.5);
          this.itemList[i-1].sprite.bringToTop();
          this.itemList[i-1].sprite.cameraOffset.setTo((xPos*this.width) - (this.itemList[i-1].sprite.width * 1) + this.x,
          (this.itemList[i-1].sprite.width * 1.5) + (yPos*this.height));
        }
      }
    }
  }
}
