import BaseGui from './BaseGui'

export default class ActionsGui extends BaseGui {
  constructor(game) {
    super(game, game.width, 0, 'inventory_gui', "Actions");
    this.slotCount = {x: 5, y: 8};
  }
}

// InventoryGui.prototype.toggle = function(game) {
//   this.bringToTop();
//   this.title.bringToTop();
//   if (this.visible == false) {
//     this.open(game);
//   } else if (this.visible == true) {
//     this.close(game);
//   }
// }

ActionsGui.prototype.setup = function(game) {
  this.scale.setTo(document.documentElement.clientHeight/this.height, document.documentElement.clientHeight/this.height);
  this.cameraOffset.setTo(game.width - this.width, 0);
  this.title.cameraOffset.setTo(this.width/2 + this.cameraOffset.x, this.title.height);
}

ActionsGui.prototype.open = function(game) {
  console.log("open");
  this.visible = true;
  this.title.visible = true;
  this.updateItemList(game);
}

ActionsGui.prototype.close = function(game) {
  console.log("close");
  this.resetItemList();
  this.visible = false;
  this.title.visible = false;
}

ActionsGui.prototype.actionOnClick = function(game) {
  console.log(this);
}

ActionsGui.prototype.resetItemList = function() {
  if (this.itemList != null) {
    for(var i = 0; i < this.itemList.length; i++) {
      // this.itemList[i].shadow.destroy(true);
      if (this.itemList[i].sprite != null) {
        this.itemList[i].sprite.destroy(true);
      }
      this.itemList[i].destroy(true);
    }
    this.itemList = null;
  } else { console.log("itemList doesn't exist"); }
  console.log(this.itemList);
}

ActionsGui.prototype.updateItemList = function(game) {
  this.resetItemList();

  this.itemList = [];
  if (this.visible) {
    for(var i = 1; i <= this.slotCount.x * this.slotCount.y; i++) { //game.world.actors[0].inventory.getItems().length
      var column = Math.floor((i-1)/this.slotCount.x); // determines the row of i
      var row = Math.floor((i-1)/this.slotCount.y); // determines the column of i
      var rowPos = (row);
      var columnPos = i-(column * this.slotCount.x);
      var xPos = columnPos / this.slotCount.x;
      var yPos = (column / this.slotCount.y) * 0.9;
      // console.log("i: " + i + " | " + "Row: " + row + " | " + "Column: " + column + " | " +
      // "RowPos: " + rowPos + " | " + "ColumnPos: " + columnPos + " | " + "xPos: " + xPos + " | " + "yPos: " + yPos + " | " + "x: " + xPos*this.width + " | " + "y: " + yPos*this.height);
      this.itemList[i-1] = game.add.button(0, 0, 'selection_gui', this.actionOnClick, this.itemList[i], 2, 1, 0); //we use getItems()[i][1] because [0] contains the values of the item, 0 contains the key.
      this.itemList[i-1].scale.setTo(this.width / (this.slotCount.x + 2) / this.itemList[i-1].width, this.width / (this.slotCount.x + 2) / this.itemList[i-1].width);
      this.itemList[i-1].fixedToCamera = true;
      this.itemList[i-1].smoothed = false;
      this.itemList[i-1].anchor.setTo(0.5);
      this.itemList[i-1].bringToTop();
      this.itemList[i-1].cameraOffset.setTo((xPos*this.width) - (this.itemList[i-1].width * 0.75) + this.x, (this.itemList[i-1].width * 1.5) + (yPos*this.height));
      // console.log(this.itemList[i].cameraOffset.y)
      if (game.world.actors[0].getActions()[i] != null) {
        this.itemList[i-1].actionKey = game.world.actors[0].getActions()[i][0];
        this.itemList[i-1].sprite = game.add.sprite(0, 0, game.world.actors[0].getActions()[i].spriteKey); //we use getItems()[i][1] because [0] contains the values of the item
        this.itemList[i-1].sprite.fixedToCamera = true;
        this.itemList[i-1].sprite.scale.setTo(this.width / (this.slotCount.x + 2) / this.itemList[i-1].sprite.width, this.width / (this.slotCount.x + 2) / this.itemList[i-1].sprite.width);
        this.itemList[i-1].sprite.smoothed = false;
        this.itemList[i-1].sprite.anchor.setTo(0.5);
        this.itemList[i-1].sprite.bringToTop();
        this.itemList[i-1].sprite.cameraOffset.setTo((xPos*this.width) - (this.itemList[i-1].sprite.width * 0.75) + this.x,
        (this.itemList[i-1].sprite.width * 1.5) + (yPos*this.height));
      }
    }
  }
}
