export default class BaseGui {
  constructor(game, x, y, key, titleText, titleSize) { // titleSize must be in standard string format eg; "20pt"
    Phaser.Sprite.call(this, game, x, y, key);
    this.fixedToCamera = true;
    this.cameraOffset.setTo(x, y);
    this.visible = false;
    this.smoothed = false;
    this.alpha = game.world.guiAlpha;
    this.title = game.add.text(0, 0, titleText);
    this.title.setShadow(3, 3, 'rgba(0,0,0,0.5)', 5);
    this.title.fixedToCamera = true;
    this.title.smoothed = false;
    this.title.anchor.setTo(0.5);
    this.title.bringToTop();
    this.title.visible = this.visible;
    this.title.font = 'Press Start 2P';
    this.checkInventory = game.world.actors[0].inventory.getItems();
    game.add.existing(this);
  }
}

BaseGui.prototype = Object.create(Phaser.Sprite.prototype);
BaseGui.prototype.constructor = BaseGui;

BaseGui.prototype.setup = function() {
  this.scale.setTo(document.documentElement.clientWidth/this.width, document.documentElement.clientHeight/this.height);
  this.title.cameraOffset.setTo(this.width/2, this.title.height);

}
BaseGui.prototype.toggle = function(game) {
  this.bringToTop();
  this.title.bringToTop();
  if (this.visible == false) {
    this.open(game);
  } else if (this.visible == true) {
    this.close(game);
  }
}

BaseGui.prototype.open = function(game) {
  console.log("open");
  this.visible = true;
  this.title.visible = true;
}

BaseGui.prototype.close = function(game) {
  console.log("close");
  this.visible = false;
  this.title.visible = false;
}

BaseGui.prototype.setAlpha = function(alpha) {
  this.alpha = alpha;
}

BaseGui.prototype.inventoryHasChanged = function(game) {
  if (this.checkInventory != null) {
    if (this.checkInventory != game.world.actors[0].inventory.containingItems) {
      // console.log(this.checkInventory)
      // console.log(game.world.actors[0].inventory.containingItems)
      // console.log(this.checkInventory != game.world.actors[0].inventory.containingItems)
      this.checkInventory = game.world.actors[0].inventory.containingItems;
      return true;
    } else { return false; }
  } else { this.checkInventory = game.world.actors[0].inventory.containingItems; }
}
