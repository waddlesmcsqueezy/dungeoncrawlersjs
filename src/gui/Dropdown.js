import BaseGui from './BaseGui';

export default class Dropdown extends BaseGui {
  constructor(game) {
    super(game, 0, 0, 'square_icon', "Options");
  }
}

Dropdown.prototype.setup = function() {
  // this.scale.setTo(document.documentElement.clientHeight/this.height, document.documentElement.clientHeight/this.height);
  this.cameraOffset.setTo(game.input.activePointer.x, game.input.activePointer.y);
  this.title.cameraOffset.setTo(this.width/2, this.title.height);

}

// Dropdown.prototype.toggle = function(game) {
//   this.bringToTop();
//   this.title.bringToTop();
//   if (this.visible == false) {
//     this.open(game);
//   } else if (this.visible == true) {
//     this.close(game);
//   }
// }

Dropdown.prototype.open = function(game, item) {
  // console.log("open");
  this.game.world.actors[0].inventory.getItemById(this.key)
  this.cameraOffset.setTo(game.input.activePointer.x, game.input.activePointer.y);
  this.visible = true;
  this.title.visible = true;
  this.updateItemList(game, item);
}

Dropdown.prototype.close = function(game) {
  // console.log("close");
  this.visible = false;
  this.title.visible = false;
}

Dropdown.prototype.toggle = function(game, item) {
  this.bringToTop();
  this.title.bringToTop();
  if (this.visible == false) {
    this.open(game, item);
  } else if (this.visible == true) {
    this.close(game);
  }
}

Dropdown.prototype.setAlpha = function(alpha) {
  this.alpha = alpha;
}

Dropdown.prototype.actionOnClick = function(game) {
  console.log(this.game.world.actors[0].inventory.getItemById(this.key));
}

Dropdown.prototype.resetItemList = function() {
  if (this.itemList != null) {
    for(var i = 0; i < this.itemList.length; i++) {
      // this.itemList[i].shadow.destroy(true);
      this.itemList[i].text.destroy(true);
      this.itemList[i].destroy(true);
    }
    this.itemList = null;
  } else { console.log("itemList doesn't exist"); }
}

Dropdown.prototype.updateItemList = function(game, item) {
  this.resetItemList();
  console.log(item)
  this.itemList = [];
  for(var i = 0; i < item[1].inventoryOptions.length; i++) {
    this.itemList[i] = game.add.button(0, 0, 'selection_slim_gui', this.actionOnClick, this.itemList[i], 2, 1, 0);
    this.itemList[i].fixedToCamera = true;
    this.itemList[i].smoothed = false;
    this.itemList[i].anchor.setTo(0.5);
    this.itemList[i].bringToTop();
    this.itemList[i].cameraOffset.setTo(this.width, this.height * i + 32);
    this.itemList[i].text = game.add.text(0, 0, item[1].inventoryOptions[i].title);
    // this.itemList[i].sprite.scale.setTo(0.7);
    this.itemList[i].text.fixedToCamera = true;
    this.itemList[i].text.smoothed = false;
    this.itemList[i].text.anchor.setTo(0.5);
    this.itemList[i].text.bringToTop();
    this.itemList[i].text.cameraOffset.setTo(this.width, this.height * i);
  }
}
