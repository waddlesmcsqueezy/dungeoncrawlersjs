import 'pixi'
import 'p2'
import Phaser from 'phaser'

import BootState from './states/Boot'
import SplashState from './states/Splash'
import MainMenuState from './states/MainMenu'
import GameState from './states/Game'

var resolution = {w: 1920, h: 1080}

class Game extends Phaser.Game {

  constructor () {
    let width = document.documentElement.clientWidth //> resolution.w ? resolution.h : document.documentElement.clientWidth
    let height = document.documentElement.clientHeight //> resolution.w ? resolution.h : document.documentElement.clientHeight

    super(width, height, Phaser.AUTO, 'content', 'Dungeon Crawlers', false, false)

    this.state.add('Boot', BootState, false)
    this.state.add('Splash', SplashState, false)
    this.state.add('MainMenu', MainMenuState, false)
    this.state.add('Game', GameState, false)

    this.state.start('Boot')
  }
}

window.game = new Game()
