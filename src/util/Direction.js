var Direction = {
  NE: {x: 1, y: -1},
  N: {x: 0, y: -1},
  NW: {x: -1, y: -1},
  E: {x: 1, y: 0},
  W: {x: -1, y: 0},
  SE: {x: 1, y: 1},
  S: {x: 0, y: 1},
  SW: {x: -1, y: 1},
}

export default Direction
