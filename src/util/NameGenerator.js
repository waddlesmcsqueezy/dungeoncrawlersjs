const intelligentPrePrefixes = preprefixes.intelligent;
const intelligentPrefixes = prefixes.intelligent;
const intelligentNames = names.intelligent;
const intelligentSuffixes = suffixes.intelligent;

const normalPrePrefixes = preprefixes.normal;
const normalPrefixes = prefixes.normal;
const normalNames = names.normal;
const normalSuffixes = suffixes.normal;

const primitivePrePrefixes = preprefixes.primitive;
const primitivePrefixes = prefixes.primitive;
const primitiveNames = names.primitive;
const primitiveSuffixes = suffixes.primitive;

function randomIndex(array) {
	return Math.floor((Math.random() * array.length) + 0)
}

function pickRandomName(prePrefixArray, prefixArray, nameArray, suffixArray) {

	prePrefixIndex = randomIndex(prePrefixArray);
	prefixIndex = randomIndex(prefixArray);
	nameIndex = randomIndex(nameArray);
	suffixIndex = randomIndex(suffixArray);

	const name = nameArray[nameIndex].name;

	if (.50 > Math.random()) {

		finalName = `${prePrefixArray[prePrefixIndex].name} ${prefixArray[prefixIndex].name} of ${name}`;
	} else {

		finalName = `${name} ${suffixArray[suffixIndex].name}`;
	}

	return finalName;
}

class Name {
	constructor(name, description, type) {
		this.name = name;
		this.description = description;
		this.type = type;
	}
}

function createRandomInhabitant(description) {

	var inhabitant;

	if (type.intelligence < 5) {
		var inhabitant = new Inhabitant(pickRandomName(primitivePrePrefixes,primitivePrefixes,
			primitiveNames,primitiveSuffixes), description, type);
	}

	if (type.intelligence < 8 && type.intelligence >= 5) {
		var inhabitant = new Inhabitant(pickRandomName(normalPrePrefixes, normalPrefixes, normalNames, normalSuffixes), description, type);
	}

	if (type.intelligence >= 8) {
		var inhabitant = new Inhabitant(pickRandomName(intelligentPrePrefixes, intelligentPrefixes,
			intelligentNames, intelligentSuffixes), description, type);
	}

	return inhabitant;
}
