import shortid from 'shortid'
import Types from '../content/Types'
import Items from '../content/Items'
import Weapon from './Weapon';
import Armor from './Armor';

export default class ItemManager {
    constructor(game) {
        this.collection = new Map()
        this.game = game
    }

    createSendItem(game, baseItem, container) {
      if (baseItem != null) {
        switch (baseItem.type) {
          case Types.itemTypes.HEAD:
            var newItem = new Armor(baseItem, game);
            break;
          case Types.itemTypes.TORSO:
            var newItem = new Armor(baseItem, game);
            break;
          case Types.itemTypes.LEG:
            var newItem = new Armor(baseItem, game);
            break;
          case Types.itemTypes.WEAPON:
            var newItem = new Weapon(baseItem, game);
            break;
          case Types.itemTypes.AMMO:
            var newItem = null;
            console.log("Add Ammo Type!")
            break;
          default:
        throw "errors.invalidWeaponType";
        }
        var id = shortid.generate();
        // console.log("Created new Item: " + id + " - stats: " + newItem);
        var itemObject = [id, newItem];
        this.sendItemToContainer(game, itemObject, container);
      } else { throw "errors.invalidItem"; }
    }

    sendItemToContainer(game, refId, container) {
        if (refId != null) {
            container.addItemToInventory(game, refId);
        } else { throw errors.invalidRefId; }
    }
}
