import Item from '.'

export default class HealthPotion extends Item {
    constructor(name, description, value, tier, sprite, health) {
        super(name, description, value, tier, sprite)
    }
}
