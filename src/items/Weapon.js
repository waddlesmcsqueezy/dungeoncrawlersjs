import Item from './Item';
import Types from '../content/Types';
import InventoryOptions from '../content/InventoryOptions';

export default class Weapon extends Item {
  constructor(item, game) {
    super(item, game);

    // this.onAttack = new Phaser.Signal();

    if (item.type != Types.itemTypes.WEAPON || item.damage == null || item.accuracy == null || item.range == null || item.skill1 == null) {
      console.log("Bad Item");
    } else {
      this.currentStance = Types.weaponStances.oneHanded;
      this.damage = item.damage;
      this.accuracy = item.accuracy;
      this.range = item.range;
      if (item.skill1 != null) {
        this.skill1 = item.skill1;
        this.skill1Scaling = item.skill1Scaling;
      };
      if (item.skill2 != null) {
        this.skill2 = item.skill2;
        this.skill2Scaling = item.skill2Scaling;
      };

      if (item.effects != null) {
        this.effect = item.effect;
      };

    	this.inventoryOptions = [
        InventoryOptions.equip,
        InventoryOptions.unEquip
    	];
    }
  }

  attack() {

    // this.onAttack.add(this.world.guiGroup[i].updateItemList, this.world.guiGroup[i]);
  }
}
