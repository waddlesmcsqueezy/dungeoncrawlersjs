export default class Item {
  constructor(item, game) {
    if (item.type == null || item.name == null || item.description == null || item.value == null || item.tier == null) {
      throw "errors.invalidItem" // + "type: " + item.type + "name: " + item.name + "desc: " + item.description + "value: " + item.value + "wgt: " + item.weight + "tier: " + item.tier
    } else {
      this.type = item.type;
      this.name = item.name;
      this.description = item.description;
      this.value = item.value;
      this.tier = item.tier;
      this.spriteKey = item.sprite;
      if (item.actions != null) {
        this.actions = item.actions;
      }
    }

    if (item.sprite != null) {
      this.sprite = game.add.sprite(0, 0, item.sprite)
      this.sprite.visible = false;
    } else if (item.sprite = null) {
      this.sprite = game.add.sprite(0, 0, 'error');
      this.sprite.visible = false;
    }
  }
}
