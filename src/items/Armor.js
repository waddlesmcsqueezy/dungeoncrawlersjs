import Item from './Item';
import Types from '../content/Types';

export default class Armor extends Item {
    constructor(item, game) {
        super(item, game)

        if (item.type != Types.itemTypes.HEAD || item.type != Types.itemTypes.TORSO || item.type != Types.itemTypes.LEG || item.protection == null || item.resistance == null) {
          console.log("bad item")
        } else {
          this.protection = item.protection;
          this.resistance = item.resistance;
        }
    }
}
