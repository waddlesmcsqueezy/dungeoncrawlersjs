import shortid from 'shortid'
import Map from 'collections/map'

export default class Container {
	constructor(){
    this.containingItems = new Map();
  }

	getItems() {
		return this.containingItems.entriesArray();
	}

  addItemToInventory(game, item) {
		if (this.getItems().length >= 40) {
			console.log("inventory is full. contains " + this.getItems()[0].length + " items");
		}
		else {
	    if (item != null) {
	      console.log("Id: " + item[0] + " | " + "Values: " + item[1]);
	      // this.containingItems[item] = item
	      this.containingItems.set(item[0], item[1]);
				game.world.guiUpdateSignal.dispatch(game);
	    } else { throw ("ERROR: invalid item") }
		}
  }

  removeItemFromInventory(game, refId) {
		if (refId != null) {
	    //if (refId.type != itemTypes.QST) {
        this.containingItems.delete(refId);
	    //} else if (refId.type == itemTypes.QST) { console.log("Can't drop quest items!"); }
		} else { throw ("ERROR: invalid item") }
  }

  transferItem(game, refId, container) {
    if (refId != null) {
      container.addItemToInventory(game, refId);
      this.removeItemFromInventory(game, refId);
    } else { throw ("ERROR: invalid item") }
  }

	getItemById(id) {
		return this.containingItems.get(id);
	}

	getItemActions(id) {
		return this.getItemById(id).actions;
	}
}
