import shortid from 'shortid'
import List from 'collections/list'

export default class ItemSlot {
  constructor(){
    this.item = new List()
  }

  equipItem(game, item, container) {
    if (this.item.toArray().length <= 0) {
      if (item != null) {
        // console.log("Id: " + item[0] + " | " + "Values: " + item[1])
        this.item.unshift(item)
        container.removeItemFromInventory(game, item[0])
        game.world.guiUpdateSignal.dispatch(game);
      } else { throw ("ERROR: invalid item") }
    } else { console.log("There is already an item equipped here.") }
  }

  unEquipItem(refId) { // don't use this directly, use transferItemToContainer instead
    if (this.getItem() != null) {
      this.item.clear();
      game.world.guiUpdateSignal.dispatch(game);
    } else { console.log("There is no item equipped here.") }
  }

  transferItemToContainer(game, container) {
    if (this.getItem()[0] != null) {
      container.addItemToInventory(game, this.getItem());
      this.unEquipItem();
    } else {throw ("ERROR: invalid item")}
  }

  transferItemToSlot(game, itemSlot) {
    if (itemSlot.getItem() == null) {
      if (this.getItem() != null) {
        var tempItem = this.getItem();
        this.unEquipItem();
        itemSlot.item.unshift(tempItem);
        game.world.guiUpdateSignal.dispatch(game);
      } else {console.log("no item in this slot")}
    } else {console.log("already an item in requested slot")}
  }

  swapItem(game, container, newItem) {
    if (newItem != null) {
      console.log(newItem);
      this.transferItemToContainer(game, container);
      this.equipItem(game, newItem, container);
    } else {console.log("bad item")}
  }

  getItem() {
    if (this.item.toArray().length > 0) {
      return this.item.toArray()[0];
    } else {  }
  }
}
