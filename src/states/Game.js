/* globals __DEV__ */
import Phaser from 'phaser';
import _ from 'lodash';
import MapFactory from '../maps/MapFactory';
import LycanClass from '../actors/LycanClass';
import EnemyClass from '../actors/EnemyClass';
import Player from '../actors/Player';
import Enemy from '../actors/Enemy';
import TurnHandler from '../actors/Turns';
import ItemManager from '../items/ItemManager';
import BattleHandler from '../combat/BattleHandler';
import BossMusic from '../sound/BossMusic';
import InventoryGui from '../gui/InventoryGui';
import ActionsGui from '../gui/ActionsGui';
import EquipmentGui from '../gui/EquipmentGui';
import Dropdown from '../gui/Dropdown';
import GuiGroup from '../gui/GuiGroup';
import Items from '../content/Items.js';
import Types from '../content/Types.js';


export default class extends Phaser.State {

  init () {
      // let playerData = [{"name" : "Tharkaad", "characterClass" : "Lycan", "agility" : "7", "strength" : "7", "speech" : "1" },
      // 	{"name" : "Turkayyid", "characterClass" : "Highwayman",  "agility" : "6", "strength" : "4", "speech" : "5"}
      // ];

  }

  preload () {
  }

  create () {

    this.world.guiUpdateSignal = new Phaser.Signal();

    this.world.guiAlpha = 0.95;

    this.world.actors = [];

    this.world.WORLD_SCALE = 64;
    this.world.setBounds(0, 0, 6400, 6400);

    this.cursors = this.input.keyboard.createCursorKeys();
    this.interact = this.input.keyboard.addKey(Phaser.KeyCode.E);
    this.stage.backgroundColor = '#000000';

    this.mapFactory = new MapFactory();
    this.mapFactory.createMap(game, 3, 3);
    this.world.itemManager = new ItemManager();
    this.world.turnHandler = new TurnHandler(game);
    this.world.battleHandler = new BattleHandler();

    this.world.actors[0] = new Player(this, new LycanClass(), this.add.sprite(0, 0, 'highwayman'));
    this.world.actors[1] = new Enemy(this, new EnemyClass(), this.add.sprite(0, 0, 'skeleton'));
    this.world.actors[2] = new Enemy(this, new EnemyClass(), this.add.sprite(0, 0, 'skeleton'));

    this.world.actors[0].setPos(3, 3);
    this.world.actors[1].setPos(9, 1);
    this.world.actors[2].setPos(7, 9);

    this.camera.follow(this.world.actors[0].sprite, Phaser.Camera.FOLLOW_LOCKON, 0.1, 0.1);

    this.world.inventoryGui = new InventoryGui(this);
    // this.world.actionsGui = new ActionsGui(this);
    // this.world.equipmentGui = new EquipmentGui(this);

    this.world.guiGroup = [];
    this.world.guiGroup.push(this.world.inventoryGui);
    // this.world.guiGroup.push(this.world.actionsGui);
    // this.world.guiGroup.push(this.world.equipmentGui);

    for(var i = 0; i < this.world.guiGroup.length; i++) {
      this.world.guiGroup[i].setup(game);
      this.world.guiUpdateSignal.add(this.world.guiGroup[i].updateItemList, this.world.guiGroup[i]);
    }

    this.world.weaponStanceGui = this.add.text(64, 64, this.world.actors[0].weaponStance + "H");
    this.world.weaponStanceGui.fixedToCamera = true;
    this.world.weaponStanceGui.font = 'Press Start 2P';
    this.world.weaponStanceGui.anchor.setTo(0.5);
    this.world.weaponStanceGui.addColor("#ffffff", 0);
    this.world.weaponStanceGui.smoothed = false;
    this.world.weaponStanceGui.cameraOffset.setTo(64, 64);

    this.world.stanceHandGui = this.add.text(64, 64, this.world.actors[0].stanceHand);
    this.world.stanceHandGui.fixedToCamera = true;
    this.world.stanceHandGui.font = 'Press Start 2P';
    this.world.stanceHandGui.anchor.setTo(0.5);
    this.world.stanceHandGui.addColor("#ffffff", 0);
    this.world.stanceHandGui.smoothed = false;
    this.world.stanceHandGui.cameraOffset.setTo(this.world.weaponStanceGui.x + this.world.weaponStanceGui.width, 64);

    this.world.itemManager.createSendItem(this, Items.WEAPON_WHITE_SWORD_001, this.world.actors[0].inventory);
    this.world.itemManager.createSendItem(this, Items.WEAPON_WHITE_AXE_001, this.world.actors[0].inventory);
    this.world.itemManager.createSendItem(this, Items.ARMOR_WHITE_TORSO_001, this.world.actors[0].inventory);
    this.world.itemManager.createSendItem(this, Items.ARMOR_WHITE_TORSO_002, this.world.actors[0].inventory);
    // this.world.itemManager.createSendItem(this, Items.WEAPON_WHITE_SWORD_001, this.world.actors[0].inventory);
    // this.world.itemManager.createSendItem(this, Items.WEAPON_WHITE_SWORD_001, this.world.actors[0].inventory);
    // this.world.itemManager.createSendItem(this, Items.WEAPON_WHITE_SWORD_001, this.world.actors[0].inventory);
    // this.world.itemManager.createSendItem(this, Items.WEAPON_WHITE_SWORD_001, this.world.actors[0].inventory);
    // this.world.itemManager.createSendItem(this, Items.WEAPON_WHITE_SWORD_001, this.world.actors[0].inventory);
    // this.world.itemManager.createSendItem(this, Items.WEAPON_WHITE_SWORD_001, this.world.actors[0].inventory);
    // this.world.itemManager.createSendItem(this, Items.WEAPON_WHITE_SWORD_001, this.world.actors[0].inventory);
    // this.world.itemManager.createSendItem(this, Items.WEAPON_WHITE_SWORD_001, this.world.actors[0].inventory);
    // this.world.itemManager.createSendItem(this, Items.WEAPON_WHITE_SWORD_001, this.world.actors[0].inventory);
    // this.world.itemManager.createSendItem(this, Items.WEAPON_WHITE_SWORD_001, this.world.actors[0].inventory);
    // this.world.itemManager.createSendItem(this, Items.WEAPON_WHITE_SWORD_001, this.world.actors[0].inventory);

    // let hint = this.add.sprite(0,0,'hint')
    // hint.scale.setTo(3,3)
    // hint.smoothed = false
    // hint.visible = false
  }

  update() {
    this.world.turnHandler.proccess(game);
    for(var i = 0; i < this.world.guiGroup.length; i++) {
      if (this.world.guiGroup[i].visible == true) {
        if (this.world.guiGroup[i].updateActionList != null) {
          if (this.world.guiGroup[i].inventoryHasChanged(this)) {
            this.world.guiGroup[i].updateActionList(this);
          }
        } else if (this.world.guiGroup[i].updateItemList != null) {
          if (this.world.guiGroup[i].inventoryHasChanged(this)) {
            this.world.guiGroup[i].updateItemList(this);
          }
        }
      }
    }
    this.world.weaponStanceGui.text = this.world.actors[0].weaponStance + "H";
    if (this.world.actors[0].weaponStance == Types.weaponStances.ONEHANDED) {
      this.world.stanceHandGui.text = "LR";
    } else { this.world.stanceHandGui.text = this.world.actors[0].stanceHand; }
    // if (__DEV__) {
    //   if (this.musicKey.justDown) {
    //     this.bossMusic.phaseChange(2);
    //   }
    // }

  }

  render () {
    if (__DEV__) {
      game.debug.text("player pos: " + this.world.actors[0].gridX() + "," + this.world.actors[0].gridY(), 70, 16 * 5);
      game.debug.text("player is moving: " + this.world.actors[0].isMoving, 70, 16 * 6)
      game.debug.text("player moves left: " + this.world.actors[0].movesLeft + "/" +
        this.world.actors[0].getMaxMoves(), 70, 16 * 7)

      game.debug.text("enemy1 moves left: " + this.world.actors[1].movesLeft + "/" +
        this.world.actors[1].getMaxMoves(), 70, 16 * 9)
      game.debug.text("enemy2 moves left: " + this.world.actors[2].movesLeft + "/" +
        this.world.actors[2].getMaxMoves(), 70, 16 * 10)
      game.debug.text("Press I for Inventory.", 70, 16 * 11)
      game.debug.text("Press A for Actions.", 70, 16 * 12)
    }
  }
}
