import Phaser from 'phaser'
import { centerGameObjects } from '../utils'

export default class extends Phaser.State {
  init () {}

  preload () {
    this.loaderBg = this.add.sprite(this.game.world.centerX, this.game.world.centerY, 'loaderBg')
    this.loaderBar = this.add.sprite(this.game.world.centerX, this.game.world.centerY, 'loaderBar')
    centerGameObjects([this.loaderBg, this.loaderBar])

    this.load.setPreloadSprite(this.loaderBar)
    //
    // load your assets
    //
    //this.load.image('mushroom', 'assets/images/mushroom2.png');
    this.load.image('highwayman', '../assets/images/highwayman_idle.png');
    this.load.image('skeleton', '../assets/images/skeleton_idle.png');
    this.load.spritesheet('spritesheet', '../assets/images/spritesheet.png', 64, 64);
    this.load.image('16_9_bg_inv', '../assets/images/16_9_bg_inv.png');
    // this.load.image('inventory_tile_gui', '../assets/images/inventory_tile.png');
    // this.load.spritesheet('selection_gui', '../assets/images/selection.png', 64, 64);
    this.load.spritesheet('selection_slim_gui', '../assets/images/selection_slim.png', 64, 16);
    // this.load.image('square_icon', '../assets/images/64x64 icon.png');
    this.load.image('error', '../assets/images/error.png');
    this.load.image('hint', '../assets/images/hint.png');
    // this.load.image('inv', '../assets/images/inv.png');
    // this.load.image('equipped', '../assets/images/equipped.png');
    this.load.image('sprite_sword', '../assets/images/sword.png');
    this.load.image('vignette_high', '../assets/images/vignettehigh.png');
    this.load.image('vignette_medium', '../assets/images/vignettemed.png');
    this.load.image('vignette_low', '../assets/images/vignettelow.png');
    this.load.image('equipped_icon', '../assets/images/equipped.png');
    this.load.image('equipped_icon_left', '../assets/images/equipped_l.png');
    this.load.image('equipped_icon_right', '../assets/images/equipped_r.png');

    this.load.tilemap('map', '../assets/map/map.json', null, Phaser.Tilemap.TILED_JSON);

    this.load.spritesheet('button', 'assets/images/mainmenu_button.png', 174, 64);
    this.load.image('mainmenu', 'assets/images/mainmenu.png');

    // this.load.audio('mainmenu_music', 'assets/sound/mainmenu.mp3');
    this.load.audio('mild_boss_music', 'assets/sound/mild_boss_music.wav');
    this.load.audio('open_inventory', 'assets/sound/open_inventory.wav');
    this.load.audio('close_inventory', 'assets/sound/close_inventory.wav');
    this.load.audio('stone_door_open', 'assets/sound/stone_door_open.wav');
    this.load.audio('gun_quiet', 'assets/sound/weapons/gun_quiet.wav')
  }

  create () {
    this.state.start('Game')
  }
}
