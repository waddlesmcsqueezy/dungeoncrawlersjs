import Phaser from 'phaser'

export default class extends Phaser.State {
  init() {}

  preload() {

  }

  create() {
    this.stage.backgroundColor = '#182d3b';

    this.background = game.add.image(0, 0, 'mainmenu');
    this.background.scale.setTo(this.game.width / this.background.width, this.game.height / this.background.height);
    console.log(this)
    console.log(this.background);

    this.button = this.add.button(0, 0, 'button', this.actionOnClick, this, 2, 1, 0);
    this.button.x = game.world.centerX - this.button.texture.frame.width / 2;
    this.button.y = game.world.centerY - 100;

    this.button.onInputOver.add(over, this);
    this.button.onInputOut.add(out, this);
    this.button.onInputUp.add(up, this);

    this.music = this.add.sound('mainmenu_music', 0.1);
    // this.music.fadeIn(1500, true);
    this.music.play('', 0, 0.1);

    this.fadeOutTime = 4000;
    this.fadeInTime = 6000;

    this.camera.flash(0x000000, this.fadeInTime, true);
  }

  update() {

  }

  render() {
  }

  stopMusic() {
    this.music.stop();
  }

  actionOnClick () {
    this.camera.fade(0x000000, 4000, true);
    this.time.events.add(4000, this.startGame, this);
  }

  startGame() {
    this.stopMusic();
    this.state.start('Game');
  }
}

function up() {
}

function over() {
}

function out() {
}
