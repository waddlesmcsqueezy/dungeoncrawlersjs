import Types from '../content/Types'

export default class BattleHandler {
  constructor() {
  }

  getHitChance(weapon, attack) {
    return weapon.accuracy * attack.accuracyMod
  }

  getAttackDamage(attacker, weapon, attack) {
    if (weapon.skill1 === null && weapon.skill2 === null) {
      var damage = weapon.damage * attack.damage
      return damage
    } else if (weapon.skill1 != null && weapon.skill2 === null) {
      var damage = (weapon.damage + (attacker.characterClass.getActorStat(weapon.skill1) * weapon.skill1Scaling)) * attack.damage
      return damage
    } else if (weapon.skill1 != null && this.weapon.skill2 != null) {
      var damage = (weapon.damage + (attacker.characterClass.getActorStat(weapon.skill1) * weapon.skill1Scaling +
        attacker.characterClass.getActorStat(weapon.skill2) * weapon.skill2Scaling)) * attack.damage
      return damage
    }

  }

  getDefenderProtection(defender) {
    var protection = defender.characterClass.getPhysicalResistance()
    return protection
  }

  runBattle(attacker, defender, weapon, attack) {
    attacker.useMoves()
    if (attacker === null || defender === null || weapon === null || attack === null) {
      throw "INVALID BATTLE SETUP"
      console.log("Attacker:", attacker, "| Defender:", defender, "| Weapon:", weapon, "| Attack:", attack)
    } else {
      if (Math.round((Math.random() * 100) + 0) < getHitChance(weapon, attack)) {
        defender.damage(getAttackDamage(attacker, weapon, attack) - getDefenderProtection(defender))
      } else {console.log(attacker, "missed the target,", defender)}
    }
  }
}
