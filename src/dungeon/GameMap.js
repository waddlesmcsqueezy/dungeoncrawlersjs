import tilemap from 'phaser'

export default class GameMap {
    constructor(tilemap, scale, wallLayer, floorLayer, objectLayer, tileSet) {
        this.wallIndex = 1
        this.floorIndex = 2
        this.objectIndex = 3

        this.tileMap = tilemap
        this.tileMap.addTilesetImage(tileSet, tileSet, scale, scale)
        this.floor = this.tileMap.createLayer(floorLayer)
        this.wall = this.tileMap.createLayer(wallLayer)
        this.objects = this.tileMap.createLayer(objectLayer)
    }

    removeObject(x, y) {
        this.tileMap.removeTileWorldXY(x, y, 1, 1, this.objects)
        this.putFloorTile(x, y)
    }

    putFloorTile(x, y) {
        this.tileMap.putTile(this.floorIndex, x, y, this.floor)
    }
}
