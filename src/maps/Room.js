import statics from '../util/Statics';

export default class Room {
  constructor(game, xPos, yPos, width, height) {
    // make internal grid
    this.grid = [];
    this.width = width;
    this.height = height;

    for (var y = 0; y < this.height; y++) {
      this.grid.push([]);
      for (var x = 0; x < this.width; x++) {
        this.grid[y].push(0);
      }
    }

    // make floor = width x height
    for (var y = 0; y < this.height; y++) {
      for (var x = 0; x < this.width; x++) {
        if (this.grid[y][x] === 0) {
          if (x == 0 || x == this.width - 1 || y == 0 || y == this.height - 1) {  //spritesheet frames: 0 = wall tile, 1 = floor tile, 2 = chest tile
            this.grid[y][x] = game.add.sprite((x * game.world.WORLD_SCALE) + (xPos * game.world.WORLD_SCALE), (y * game.world.WORLD_SCALE) + (yPos * game.world.WORLD_SCALE), 'spritesheet', 0); // wall
            this.grid[y][x].isSolid = true;
          } else {
            if ((Math.random() * (100 - 0) + 0) > 7) { // 7% chance to make a solid tile
              this.grid[y][x] = game.add.sprite((x * game.world.WORLD_SCALE) + (xPos * game.world.WORLD_SCALE),
              (y * game.world.WORLD_SCALE) + (yPos * game.world.WORLD_SCALE), 'spritesheet', 1);
              this.grid[y][x].isSolid = false;
            } else {
              this.grid[y][x] = game.add.sprite((x * game.world.WORLD_SCALE) + (xPos * game.world.WORLD_SCALE), (y * game.world.WORLD_SCALE) + (yPos * game.world.WORLD_SCALE), 'spritesheet', 0); // wall
              this.grid[y][x].isSolid = true;
            }
          } //floor
        } else {console.log("Room grid: " + x, y + " is occupied by: " + game.world.map[y][x])}
      }
    }
  }

  getTile(x, y) {
    return this.grid[y][x];
  }
}
