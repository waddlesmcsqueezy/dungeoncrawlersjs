# README #

This README would normally document whatever steps are necessary to get your application up and running.

### What is this repository for? ###

* This the private repository for the DungeonCrawlersJS Game
* DungeonCrawlers 1.1.0
* [Learn Markdown](https://bitbucket.org/tutorials/markdowndemo)
* Project setup from [phaser-es6-webpack](https://github.com/lean/phaser-es6-webpack)

### How do I get set up? ###

* Summary of set up
Install node & npm

```
npm install
npm run dev
```
Runs a dev server on http://localhost:3000.
Watches for file changes and automatically reloads the browser.

* Configuration
* Dependencies

To install new npm dependencies:

```
npm install xxx --save
```

* Database configuration
* How to run tests
* Deployment instructions


### Contribution guidelines ###

* Writing tests
* Code review
* Other guidelines

### Who do I talk to? ###

* Ian Harmon @ ian@wolvesinexile.com